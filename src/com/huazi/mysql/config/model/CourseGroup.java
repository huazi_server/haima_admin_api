package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * CourseGroup entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "course_group", catalog = "haimawang")
public class CourseGroup extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private String name;
	private String issue;
	private Integer category;
	private Double price;
	private Double realPrice;
	private Integer enableFlag;
	private Integer courseGroupFlag;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public CourseGroup() {
	}

	/** minimal constructor */
	public CourseGroup(Integer delFlag) {
		this.delFlag = delFlag;
	}

	/** full constructor */
	public CourseGroup(String name, String issue, Integer category,
			Double price, Double realPrice, Integer enableFlag,
			Integer courseGroupFlag, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.name = name;
		this.issue = issue;
		this.category = category;
		this.price = price;
		this.realPrice = realPrice;
		this.enableFlag = enableFlag;
		this.courseGroupFlag = courseGroupFlag;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 1000)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "issue", length = 32)
	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	@Column(name = "category")
	public Integer getCategory() {
		return this.category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	@Column(name = "price", precision = 10)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "realPrice", precision = 10)
	public Double getRealPrice() {
		return this.realPrice;
	}

	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	@Column(name = "enableFlag")
	public Integer getEnableFlag() {
		return this.enableFlag;
	}

	public void setEnableFlag(Integer enableFlag) {
		this.enableFlag = enableFlag;
	}

	@Column(name = "courseGroupFlag")
	public Integer getCourseGroupFlag() {
		return this.courseGroupFlag;
	}

	public void setCourseGroupFlag(Integer courseGroupFlag) {
		this.courseGroupFlag = courseGroupFlag;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}