package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * ChannelStatistics entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "channel_statistics", catalog = "haimawang")
public class ChannelStatistics extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long channelId;
	private Long studentId;
	private String phone;
	private Integer sourceType;
	private Long sourceId;
	private Long courseId;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public ChannelStatistics() {
	}

	/** minimal constructor */
	public ChannelStatistics(Long channelId, Long studentId, Integer delFlag) {
		this.channelId = channelId;
		this.studentId = studentId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public ChannelStatistics(Long channelId, Long studentId, String phone,
			Integer sourceType, Long sourceId, Long courseId, String remark,
			Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.channelId = channelId;
		this.studentId = studentId;
		this.phone = phone;
		this.sourceType = sourceType;
		this.sourceId = sourceId;
		this.courseId = courseId;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "channelId", nullable = false)
	public Long getChannelId() {
		return this.channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "studentId", nullable = false)
	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Column(name = "phone", length = 32)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "sourceType")
	public Integer getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "sourceId")
	public Long getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "courseId")
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}