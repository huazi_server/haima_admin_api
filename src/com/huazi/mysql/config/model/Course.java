package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Course entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "course", catalog = "haimawang")
public class Course extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long coursePackageId;
	private Long courseGroupId;
	private String name;
	private String coverUrl;
	private Integer courseType;
	private Integer courseFlag;
	private String issue;
	private String lessonDescription;
	private Timestamp signStartAt;
	private Timestamp signEndAt;
	private Timestamp teachStartAt;
	private Timestamp teachEndAt;
	private Integer stage;
	private Integer category;
	private Integer enrollCount;
	private Integer enableFlag;
	private String planTeacherIds;
	private Double price;
	private Double realPrice;
	private Long operateUserId;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public Course() {
	}

	/** minimal constructor */
	public Course(Long coursePackageId, Integer delFlag) {
		this.coursePackageId = coursePackageId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public Course(Long coursePackageId, Long courseGroupId, String name,
			String coverUrl, Integer courseType, Integer courseFlag,
			String issue, String lessonDescription, Timestamp signStartAt,
			Timestamp signEndAt, Timestamp teachStartAt, Timestamp teachEndAt,
			Integer stage, Integer category, Integer enrollCount,
			Integer enableFlag, String planTeacherIds, Double price,
			Double realPrice, Long operateUserId, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.coursePackageId = coursePackageId;
		this.courseGroupId = courseGroupId;
		this.name = name;
		this.coverUrl = coverUrl;
		this.courseType = courseType;
		this.courseFlag = courseFlag;
		this.issue = issue;
		this.lessonDescription = lessonDescription;
		this.signStartAt = signStartAt;
		this.signEndAt = signEndAt;
		this.teachStartAt = teachStartAt;
		this.teachEndAt = teachEndAt;
		this.stage = stage;
		this.category = category;
		this.enrollCount = enrollCount;
		this.enableFlag = enableFlag;
		this.planTeacherIds = planTeacherIds;
		this.price = price;
		this.realPrice = realPrice;
		this.operateUserId = operateUserId;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "coursePackageId", nullable = false)
	public Long getCoursePackageId() {
		return this.coursePackageId;
	}

	public void setCoursePackageId(Long coursePackageId) {
		this.coursePackageId = coursePackageId;
	}

	@Column(name = "courseGroupId")
	public Long getCourseGroupId() {
		return this.courseGroupId;
	}

	public void setCourseGroupId(Long courseGroupId) {
		this.courseGroupId = courseGroupId;
	}

	@Column(name = "name", length = 1000)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "coverUrl", length = 1000)
	public String getCoverUrl() {
		return this.coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	@Column(name = "courseType")
	public Integer getCourseType() {
		return this.courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	@Column(name = "courseFlag")
	public Integer getCourseFlag() {
		return this.courseFlag;
	}

	public void setCourseFlag(Integer courseFlag) {
		this.courseFlag = courseFlag;
	}

	@Column(name = "issue", length = 32)
	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	@Column(name = "lessonDescription")
	public String getLessonDescription() {
		return this.lessonDescription;
	}

	public void setLessonDescription(String lessonDescription) {
		this.lessonDescription = lessonDescription;
	}

	@Column(name = "signStartAt", length = 19)
	public Timestamp getSignStartAt() {
		return this.signStartAt;
	}

	public void setSignStartAt(Timestamp signStartAt) {
		this.signStartAt = signStartAt;
	}

	@Column(name = "signEndAt", length = 19)
	public Timestamp getSignEndAt() {
		return this.signEndAt;
	}

	public void setSignEndAt(Timestamp signEndAt) {
		this.signEndAt = signEndAt;
	}

	@Column(name = "teachStartAt", length = 19)
	public Timestamp getTeachStartAt() {
		return this.teachStartAt;
	}

	public void setTeachStartAt(Timestamp teachStartAt) {
		this.teachStartAt = teachStartAt;
	}

	@Column(name = "teachEndAt", length = 19)
	public Timestamp getTeachEndAt() {
		return this.teachEndAt;
	}

	public void setTeachEndAt(Timestamp teachEndAt) {
		this.teachEndAt = teachEndAt;
	}

	@Column(name = "stage")
	public Integer getStage() {
		return this.stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	@Column(name = "category")
	public Integer getCategory() {
		return this.category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	@Column(name = "enrollCount")
	public Integer getEnrollCount() {
		return this.enrollCount;
	}

	public void setEnrollCount(Integer enrollCount) {
		this.enrollCount = enrollCount;
	}

	@Column(name = "enableFlag")
	public Integer getEnableFlag() {
		return this.enableFlag;
	}

	public void setEnableFlag(Integer enableFlag) {
		this.enableFlag = enableFlag;
	}

	@Column(name = "planTeacherIds", length = 1000)
	public String getPlanTeacherIds() {
		return this.planTeacherIds;
	}

	public void setPlanTeacherIds(String planTeacherIds) {
		this.planTeacherIds = planTeacherIds;
	}

	@Column(name = "price", precision = 10)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "realPrice", precision = 10)
	public Double getRealPrice() {
		return this.realPrice;
	}

	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	@Column(name = "operateUserId")
	public Long getOperateUserId() {
		return this.operateUserId;
	}

	public void setOperateUserId(Long operateUserId) {
		this.operateUserId = operateUserId;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}