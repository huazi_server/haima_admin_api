package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;


/**
 * ReportConfig entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="report_config"
    ,catalog="haimawang"
)

public class ReportConfig  extends BaseEntity implements java.io.Serializable {


    // Fields    

     private Long id;
     private Integer type;
     private String content;
     private String remark;
     private Integer state;
     private Timestamp createdAt;
     private Timestamp updatedAt;
     private Integer delFlag;


    // Constructors

    /** default constructor */
    public ReportConfig() {
    }

	/** minimal constructor */
    public ReportConfig(Integer delFlag) {
        this.delFlag = delFlag;
    }
    
    /** full constructor */
    public ReportConfig(Integer type, String content, String remark, Integer state, Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
        this.type = type;
        this.content = content;
        this.remark = remark;
        this.state = state;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.delFlag = delFlag;
    }

   
    // Property accessors
    @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="id", unique=true, nullable=false)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="type")

    public Integer getType() {
        return this.type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    @Column(name="content", length=1000)

    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Column(name="remark", length=1000)

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    @Column(name="state")

    public Integer getState() {
        return this.state;
    }
    
    public void setState(Integer state) {
        this.state = state;
    }
    
    @Column(name="createdAt", length=19)

    public Timestamp getCreatedAt() {
        return this.createdAt;
    }
    
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
    
    @Column(name="updatedAt", length=19)

    public Timestamp getUpdatedAt() {
        return this.updatedAt;
    }
    
    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    @Column(name="delFlag", nullable=false)

    public Integer getDelFlag() {
        return this.delFlag;
    }
    
    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
   








}