package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * ActivityTuan entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "activity_tuan", catalog = "haimawang")
public class ActivityTuan extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private String name;
	private String dest;
	private String intro;
	private Integer restrictionCount;
	private Double price;
	private Timestamp startAt;
	private Timestamp endAt;
	private Integer courseType;
	private String courseGroupIds;
	private String courseIds;
	private String imgUrl;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public ActivityTuan() {
	}

	/** minimal constructor */
	public ActivityTuan(Integer delFlag) {
		this.delFlag = delFlag;
	}

	/** full constructor */
	public ActivityTuan(String name, String dest, String intro,
			Integer restrictionCount, Double price, Timestamp startAt,
			Timestamp endAt, Integer courseType, String courseGroupIds,
			String courseIds, String imgUrl, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.name = name;
		this.dest = dest;
		this.intro = intro;
		this.restrictionCount = restrictionCount;
		this.price = price;
		this.startAt = startAt;
		this.endAt = endAt;
		this.courseType = courseType;
		this.courseGroupIds = courseGroupIds;
		this.courseIds = courseIds;
		this.imgUrl = imgUrl;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "dest", length = 100)
	public String getDest() {
		return this.dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	@Column(name = "intro", length = 1000)
	public String getIntro() {
		return this.intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@Column(name = "restrictionCount")
	public Integer getRestrictionCount() {
		return this.restrictionCount;
	}

	public void setRestrictionCount(Integer restrictionCount) {
		this.restrictionCount = restrictionCount;
	}

	@Column(name = "price", precision = 10)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "startAt", length = 19)
	public Timestamp getStartAt() {
		return this.startAt;
	}

	public void setStartAt(Timestamp startAt) {
		this.startAt = startAt;
	}

	@Column(name = "endAt", length = 19)
	public Timestamp getEndAt() {
		return this.endAt;
	}

	public void setEndAt(Timestamp endAt) {
		this.endAt = endAt;
	}

	@Column(name = "courseType")
	public Integer getCourseType() {
		return this.courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	@Column(name = "courseGroupIds", length = 1000)
	public String getCourseGroupIds() {
		return this.courseGroupIds;
	}

	public void setCourseGroupIds(String courseGroupIds) {
		this.courseGroupIds = courseGroupIds;
	}

	@Column(name = "courseIds", length = 1000)
	public String getCourseIds() {
		return this.courseIds;
	}

	public void setCourseIds(String courseIds) {
		this.courseIds = courseIds;
	}

	@Column(name = "imgUrl", length = 1000)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}