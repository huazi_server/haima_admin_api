package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * ActivityFree entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "activity_free", catalog = "haimawang")
public class ActivityFree extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private String name;
	private String dest;
	private String intro;
	private Integer quotaCount;
	private Integer gainCount;
	private Integer lookCount;
	private Integer courseType;
	private Long courseGroupId;
	private Long courseId;
	private String imgUrl;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public ActivityFree() {
	}

	/** minimal constructor */
	public ActivityFree(Integer delFlag) {
		this.delFlag = delFlag;
	}

	/** full constructor */
	public ActivityFree(String name, String dest, String intro,
			Integer quotaCount, Integer gainCount, Integer lookCount,
			Integer courseType, Long courseGroupId, Long courseId,
			String imgUrl, String remark, Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.name = name;
		this.dest = dest;
		this.intro = intro;
		this.quotaCount = quotaCount;
		this.gainCount = gainCount;
		this.lookCount = lookCount;
		this.courseType = courseType;
		this.courseGroupId = courseGroupId;
		this.courseId = courseId;
		this.imgUrl = imgUrl;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "dest", length = 100)
	public String getDest() {
		return this.dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	@Column(name = "intro", length = 1000)
	public String getIntro() {
		return this.intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@Column(name = "quotaCount")
	public Integer getQuotaCount() {
		return this.quotaCount;
	}

	public void setQuotaCount(Integer quotaCount) {
		this.quotaCount = quotaCount;
	}

	@Column(name = "gainCount")
	public Integer getGainCount() {
		return this.gainCount;
	}

	public void setGainCount(Integer gainCount) {
		this.gainCount = gainCount;
	}

	@Column(name = "lookCount")
	public Integer getLookCount() {
		return this.lookCount;
	}

	public void setLookCount(Integer lookCount) {
		this.lookCount = lookCount;
	}

	@Column(name = "courseType")
	public Integer getCourseType() {
		return this.courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	@Column(name = "courseGroupId")
	public Long getCourseGroupId() {
		return this.courseGroupId;
	}

	public void setCourseGroupId(Long courseGroupId) {
		this.courseGroupId = courseGroupId;
	}

	@Column(name = "courseId")
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "imgUrl", length = 1000)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}