package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Report entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "report", catalog = "haimawang")
public class Report extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long reportConfigId;
	private Integer type;
	private Long defendantId;
	private Long plaintiffId;
	private String title;
	private String content;
	private String imgUrl;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public Report() {
	}

	/** minimal constructor */
	public Report(Long plaintiffId, Integer delFlag) {
		this.plaintiffId = plaintiffId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public Report(Long reportConfigId, Integer type, Long defendantId,
			Long plaintiffId, String title, String content, String imgUrl,
			String remark, Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.reportConfigId = reportConfigId;
		this.type = type;
		this.defendantId = defendantId;
		this.plaintiffId = plaintiffId;
		this.title = title;
		this.content = content;
		this.imgUrl = imgUrl;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "reportConfigId")
	public Long getReportConfigId() {
		return this.reportConfigId;
	}

	public void setReportConfigId(Long reportConfigId) {
		this.reportConfigId = reportConfigId;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "defendantId")
	public Long getDefendantId() {
		return this.defendantId;
	}

	public void setDefendantId(Long defendantId) {
		this.defendantId = defendantId;
	}

	@Column(name = "plaintiffId", nullable = false)
	public Long getPlaintiffId() {
		return this.plaintiffId;
	}

	public void setPlaintiffId(Long plaintiffId) {
		this.plaintiffId = plaintiffId;
	}

	@Column(name = "title", length = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "content", length = 1000)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "imgUrl", length = 1000)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}