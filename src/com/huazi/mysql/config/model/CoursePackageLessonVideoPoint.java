package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * CoursePackageLessonVideoPoint entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "course_package_lesson_video_point", catalog = "haimawang")
public class CoursePackageLessonVideoPoint extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long coursePackageId;
	private Long coursePackageLessonId;
	private Integer number;
	private Integer startTime;
	private Integer endTime;
	private String title;
	private String imgUrl;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public CoursePackageLessonVideoPoint() {
	}

	/** minimal constructor */
	public CoursePackageLessonVideoPoint(Long coursePackageId,
			Long coursePackageLessonId, Integer delFlag) {
		this.coursePackageId = coursePackageId;
		this.coursePackageLessonId = coursePackageLessonId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public CoursePackageLessonVideoPoint(Long coursePackageId,
			Long coursePackageLessonId, Integer number, Integer startTime,
			Integer endTime, String title, String imgUrl, String remark,
			Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.coursePackageId = coursePackageId;
		this.coursePackageLessonId = coursePackageLessonId;
		this.number = number;
		this.startTime = startTime;
		this.endTime = endTime;
		this.title = title;
		this.imgUrl = imgUrl;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "coursePackageId", nullable = false)
	public Long getCoursePackageId() {
		return this.coursePackageId;
	}

	public void setCoursePackageId(Long coursePackageId) {
		this.coursePackageId = coursePackageId;
	}

	@Column(name = "coursePackageLessonId", nullable = false)
	public Long getCoursePackageLessonId() {
		return this.coursePackageLessonId;
	}

	public void setCoursePackageLessonId(Long coursePackageLessonId) {
		this.coursePackageLessonId = coursePackageLessonId;
	}

	@Column(name = "number")
	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Column(name = "startTime")
	public Integer getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}

	@Column(name = "endTime")
	public Integer getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	@Column(name = "title", length = 1000)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "imgUrl", length = 1000)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}