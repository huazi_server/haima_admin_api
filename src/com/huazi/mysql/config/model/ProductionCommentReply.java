package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * ProductionCommentReply entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "production_comment_reply", catalog = "haimawang")
public class ProductionCommentReply extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long studentId;
	private Integer type;
	private Long productionId;
	private Long commentId;
	private Long targetId;
	private Long fromId;
	private String fromName;
	private String fromAvatarUrl;
	private Long toId;
	private String toName;
	private String toAvatarUrl;
	private String content;
	private Integer likeCount;
	private Integer replyCount;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public ProductionCommentReply() {
	}

	/** minimal constructor */
	public ProductionCommentReply(Long studentId, Integer delFlag) {
		this.studentId = studentId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public ProductionCommentReply(Long studentId, Integer type,
			Long productionId, Long commentId, Long targetId, Long fromId,
			String fromName, String fromAvatarUrl, Long toId, String toName,
			String toAvatarUrl, String content, Integer likeCount,
			Integer replyCount, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.studentId = studentId;
		this.type = type;
		this.productionId = productionId;
		this.commentId = commentId;
		this.targetId = targetId;
		this.fromId = fromId;
		this.fromName = fromName;
		this.fromAvatarUrl = fromAvatarUrl;
		this.toId = toId;
		this.toName = toName;
		this.toAvatarUrl = toAvatarUrl;
		this.content = content;
		this.likeCount = likeCount;
		this.replyCount = replyCount;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "studentId", nullable = false)
	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "productionId")
	public Long getProductionId() {
		return this.productionId;
	}

	public void setProductionId(Long productionId) {
		this.productionId = productionId;
	}

	@Column(name = "commentId")
	public Long getCommentId() {
		return this.commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	@Column(name = "targetId")
	public Long getTargetId() {
		return this.targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	@Column(name = "fromId")
	public Long getFromId() {
		return this.fromId;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	@Column(name = "fromName", length = 32)
	public String getFromName() {
		return this.fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	@Column(name = "fromAvatarUrl", length = 1000)
	public String getFromAvatarUrl() {
		return this.fromAvatarUrl;
	}

	public void setFromAvatarUrl(String fromAvatarUrl) {
		this.fromAvatarUrl = fromAvatarUrl;
	}

	@Column(name = "toId")
	public Long getToId() {
		return this.toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

	@Column(name = "toName", length = 32)
	public String getToName() {
		return this.toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	@Column(name = "toAvatarUrl", length = 1000)
	public String getToAvatarUrl() {
		return this.toAvatarUrl;
	}

	public void setToAvatarUrl(String toAvatarUrl) {
		this.toAvatarUrl = toAvatarUrl;
	}

	@Column(name = "content", length = 500)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "likeCount")
	public Integer getLikeCount() {
		return this.likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	@Column(name = "replyCount")
	public Integer getReplyCount() {
		return this.replyCount;
	}

	public void setReplyCount(Integer replyCount) {
		this.replyCount = replyCount;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}