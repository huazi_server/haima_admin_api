package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Production entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "production", catalog = "haimawang")
public class Production extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long personId;
	private Integer sourceType;
	private String name;
	private String content;
	private String coverUrl;
	private Integer lookCount;
	private Integer likeCount;
	private Integer commentCount;
	private String intro;
	private String operateExplain;
	private Integer score;
	private String tagIds;
	private String tagNames;
	private Integer openFlag;
	private Integer category;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public Production() {
	}

	/** minimal constructor */
	public Production(Integer sourceType, Integer delFlag) {
		this.sourceType = sourceType;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public Production(Long personId, Integer sourceType, String name,
			String content, String coverUrl, Integer lookCount,
			Integer likeCount, Integer commentCount, String intro,
			String operateExplain, Integer score, String tagIds,
			String tagNames, Integer openFlag, Integer category, String remark,
			Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.personId = personId;
		this.sourceType = sourceType;
		this.name = name;
		this.content = content;
		this.coverUrl = coverUrl;
		this.lookCount = lookCount;
		this.likeCount = likeCount;
		this.commentCount = commentCount;
		this.intro = intro;
		this.operateExplain = operateExplain;
		this.score = score;
		this.tagIds = tagIds;
		this.tagNames = tagNames;
		this.openFlag = openFlag;
		this.category = category;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "personId")
	public Long getPersonId() {
		return this.personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	@Column(name = "sourceType", nullable = false)
	public Integer getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "name", length = 1000)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "content", length = 1000)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "coverUrl", length = 1000)
	public String getCoverUrl() {
		return this.coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	@Column(name = "lookCount")
	public Integer getLookCount() {
		return this.lookCount;
	}

	public void setLookCount(Integer lookCount) {
		this.lookCount = lookCount;
	}

	@Column(name = "likeCount")
	public Integer getLikeCount() {
		return this.likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	@Column(name = "commentCount")
	public Integer getCommentCount() {
		return this.commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	@Column(name = "intro", length = 1000)
	public String getIntro() {
		return this.intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@Column(name = "operateExplain", length = 1000)
	public String getOperateExplain() {
		return this.operateExplain;
	}

	public void setOperateExplain(String operateExplain) {
		this.operateExplain = operateExplain;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "tagIds", length = 1000)
	public String getTagIds() {
		return this.tagIds;
	}

	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	@Column(name = "tagNames", length = 1000)
	public String getTagNames() {
		return this.tagNames;
	}

	public void setTagNames(String tagNames) {
		this.tagNames = tagNames;
	}

	@Column(name = "openFlag")
	public Integer getOpenFlag() {
		return this.openFlag;
	}

	public void setOpenFlag(Integer openFlag) {
		this.openFlag = openFlag;
	}

	@Column(name = "category")
	public Integer getCategory() {
		return this.category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}