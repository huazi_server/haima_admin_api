package com.huazi.mysql.config.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * CourseLesson entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "course_lesson", catalog = "haimawang")
public class CourseLesson extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long courseId;
	private Long coursePackageId;
	private Long coursePackageLessonId;
	private Timestamp startAt;
	private Timestamp endAt;
	private Integer timeMinute;
	private String title;
	private String name;
	private String knowledges;
	private String shortKnowledges;
	private Integer number;
	private Integer enableFlag;
	private String coverUrl;
	private String videoUrl;
	private String docUrl;
	private String imgUrl;
	private String textbookUrl;
	private String sb3url;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public CourseLesson() {
	}

	/** minimal constructor */
	public CourseLesson(Long courseId, Long coursePackageId,
			Long coursePackageLessonId, Integer delFlag) {
		this.courseId = courseId;
		this.coursePackageId = coursePackageId;
		this.coursePackageLessonId = coursePackageLessonId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public CourseLesson(Long courseId, Long coursePackageId,
			Long coursePackageLessonId, Timestamp startAt, Timestamp endAt,
			Integer timeMinute, String title, String name, String knowledges,
			String shortKnowledges, Integer number, Integer enableFlag,
			String coverUrl, String videoUrl, String docUrl, String imgUrl,
			String textbookUrl, String sb3url, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.courseId = courseId;
		this.coursePackageId = coursePackageId;
		this.coursePackageLessonId = coursePackageLessonId;
		this.startAt = startAt;
		this.endAt = endAt;
		this.timeMinute = timeMinute;
		this.title = title;
		this.name = name;
		this.knowledges = knowledges;
		this.shortKnowledges = shortKnowledges;
		this.number = number;
		this.enableFlag = enableFlag;
		this.coverUrl = coverUrl;
		this.videoUrl = videoUrl;
		this.docUrl = docUrl;
		this.imgUrl = imgUrl;
		this.textbookUrl = textbookUrl;
		this.sb3url = sb3url;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "courseId", nullable = false)
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "coursePackageId", nullable = false)
	public Long getCoursePackageId() {
		return this.coursePackageId;
	}

	public void setCoursePackageId(Long coursePackageId) {
		this.coursePackageId = coursePackageId;
	}

	@Column(name = "coursePackageLessonId", nullable = false)
	public Long getCoursePackageLessonId() {
		return this.coursePackageLessonId;
	}

	public void setCoursePackageLessonId(Long coursePackageLessonId) {
		this.coursePackageLessonId = coursePackageLessonId;
	}

	@Column(name = "startAt", length = 19)
	public Timestamp getStartAt() {
		return this.startAt;
	}

	public void setStartAt(Timestamp startAt) {
		this.startAt = startAt;
	}

	@Column(name = "endAt", length = 19)
	public Timestamp getEndAt() {
		return this.endAt;
	}

	public void setEndAt(Timestamp endAt) {
		this.endAt = endAt;
	}

	@Column(name = "timeMinute")
	public Integer getTimeMinute() {
		return this.timeMinute;
	}

	public void setTimeMinute(Integer timeMinute) {
		this.timeMinute = timeMinute;
	}

	@Column(name = "title", length = 1000)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "name", length = 1000)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "knowledges", length = 1000)
	public String getKnowledges() {
		return this.knowledges;
	}

	public void setKnowledges(String knowledges) {
		this.knowledges = knowledges;
	}

	@Column(name = "shortKnowledges", length = 1000)
	public String getShortKnowledges() {
		return this.shortKnowledges;
	}

	public void setShortKnowledges(String shortKnowledges) {
		this.shortKnowledges = shortKnowledges;
	}

	@Column(name = "number")
	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Column(name = "enableFlag")
	public Integer getEnableFlag() {
		return this.enableFlag;
	}

	public void setEnableFlag(Integer enableFlag) {
		this.enableFlag = enableFlag;
	}

	@Column(name = "coverUrl", length = 1000)
	public String getCoverUrl() {
		return this.coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	@Column(name = "videoUrl", length = 1000)
	public String getVideoUrl() {
		return this.videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	@Column(name = "docUrl", length = 1000)
	public String getDocUrl() {
		return this.docUrl;
	}

	public void setDocUrl(String docUrl) {
		this.docUrl = docUrl;
	}

	@Column(name = "imgUrl", length = 65535)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "textbookUrl", length = 1000)
	public String getTextbookUrl() {
		return this.textbookUrl;
	}

	public void setTextbookUrl(String textbookUrl) {
		this.textbookUrl = textbookUrl;
	}

	@Column(name = "sb3Url", length = 1000)
	public String getSb3url() {
		return this.sb3url;
	}

	public void setSb3url(String sb3url) {
		this.sb3url = sb3url;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}