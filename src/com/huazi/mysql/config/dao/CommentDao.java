package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.Comment;


@Component
public class CommentDao extends BaseDao<Comment> {

	private static final Logger log = LoggerFactory
			.getLogger(CommentDao.class);

	@Override
	public Class<Comment> getEntityType() {
		return Comment.class;
	}

}
