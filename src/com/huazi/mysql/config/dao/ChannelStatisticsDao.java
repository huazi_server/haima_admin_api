package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ChannelStatistics;


@Component
public class ChannelStatisticsDao extends BaseDao<ChannelStatistics> {

	private static final Logger log = LoggerFactory
			.getLogger(ChannelStatisticsDao.class);

	@Override
	public Class<ChannelStatistics> getEntityType() {
		return ChannelStatistics.class;
	}

}
