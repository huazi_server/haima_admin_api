package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ReportConfig;


@Component
public class ReportConfigDao extends BaseDao<ReportConfig> {

	private static final Logger log = LoggerFactory
			.getLogger(ReportConfigDao.class);

	@Override
	public Class<ReportConfig> getEntityType() {
		return ReportConfig.class;
	}

}
