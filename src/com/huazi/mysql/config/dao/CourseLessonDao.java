package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.CourseLesson;


@Component
public class CourseLessonDao extends BaseDao<CourseLesson> {

	private static final Logger log = LoggerFactory
			.getLogger(CourseLessonDao.class);

	@Override
	public Class<CourseLesson> getEntityType() {
		return CourseLesson.class;
	}

}
