package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ProductionCommentReply;


@Component
public class ProductionCommentReplyDao extends BaseDao<ProductionCommentReply> {

	private static final Logger log = LoggerFactory
			.getLogger(ProductionCommentReplyDao.class);

	@Override
	public Class<ProductionCommentReply> getEntityType() {
		return ProductionCommentReply.class;
	}

}
