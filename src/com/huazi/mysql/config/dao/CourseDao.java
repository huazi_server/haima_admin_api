package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.Course;


@Component
public class CourseDao extends BaseDao<Course> {

	private static final Logger log = LoggerFactory
			.getLogger(CourseDao.class);

	@Override
	public Class<Course> getEntityType() {
		return Course.class;
	}

}
