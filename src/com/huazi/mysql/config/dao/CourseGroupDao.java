package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.CourseGroup;


@Component
public class CourseGroupDao extends BaseDao<CourseGroup> {

	private static final Logger log = LoggerFactory
			.getLogger(CourseGroupDao.class);

	@Override
	public Class<CourseGroup> getEntityType() {
		return CourseGroup.class;
	}

}
