package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.CoursePackageLessonVideoPoint;


@Component
public class CoursePackageLessonVideoPointDao extends BaseDao<CoursePackageLessonVideoPoint> {

	private static final Logger log = LoggerFactory
			.getLogger(CoursePackageLessonVideoPointDao.class);

	@Override
	public Class<CoursePackageLessonVideoPoint> getEntityType() {
		return CoursePackageLessonVideoPoint.class;
	}

}
