package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ChannelActivity;


@Component
public class ChannelActivityDao extends BaseDao<ChannelActivity> {

	private static final Logger log = LoggerFactory
			.getLogger(ChannelActivityDao.class);

	@Override
	public Class<ChannelActivity> getEntityType() {
		return ChannelActivity.class;
	}

}
