package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ProductionComment;


@Component
public class ProductionCommentDao extends BaseDao<ProductionComment> {

	private static final Logger log = LoggerFactory
			.getLogger(ProductionCommentDao.class);

	@Override
	public Class<ProductionComment> getEntityType() {
		return ProductionComment.class;
	}

}
