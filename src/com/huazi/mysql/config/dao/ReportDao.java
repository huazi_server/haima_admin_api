package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.Comment;
import com.huazi.mysql.config.model.Report;


@Component
public class ReportDao extends BaseDao<Report> {

	private static final Logger log = LoggerFactory
			.getLogger(ReportDao.class);

	@Override
	public Class<Report> getEntityType() {
		return Report.class;
	}

}
