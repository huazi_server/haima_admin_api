package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.CoursePackageLesson;


@Component
public class CoursePackageLessonDao extends BaseDao<CoursePackageLesson> {

	private static final Logger log = LoggerFactory
			.getLogger(CoursePackageLessonDao.class);

	@Override
	public Class<CoursePackageLesson> getEntityType() {
		return CoursePackageLesson.class;
	}

}
