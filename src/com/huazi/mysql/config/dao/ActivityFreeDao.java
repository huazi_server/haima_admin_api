package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ActivityFree;


@Component
public class ActivityFreeDao extends BaseDao<ActivityFree> {

	private static final Logger log = LoggerFactory
			.getLogger(ActivityFreeDao.class);

	@Override
	public Class<ActivityFree> getEntityType() {
		return ActivityFree.class;
	}

}
