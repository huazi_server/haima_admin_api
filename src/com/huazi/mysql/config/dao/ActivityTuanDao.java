package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.ActivityTuan;


@Component
public class ActivityTuanDao extends BaseDao<ActivityTuan> {

	private static final Logger log = LoggerFactory
			.getLogger(ActivityTuanDao.class);

	@Override
	public Class<ActivityTuan> getEntityType() {
		return ActivityTuan.class;
	}

}
