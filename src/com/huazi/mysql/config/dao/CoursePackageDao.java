package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.CoursePackage;


@Component
public class CoursePackageDao extends BaseDao<CoursePackage> {

	private static final Logger log = LoggerFactory
			.getLogger(CoursePackageDao.class);

	@Override
	public Class<CoursePackage> getEntityType() {
		return CoursePackage.class;
	}

}
