package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.Production;


@Component
public class ProductionDao extends BaseDao<Production> {

	private static final Logger log = LoggerFactory
			.getLogger(ProductionDao.class);

	@Override
	public Class<Production> getEntityType() {
		return Production.class;
	}

}
