package com.huazi.mysql.config.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.config.model.Channel;


@Component
public class ChannelDao extends BaseDao<Channel> {

	private static final Logger log = LoggerFactory
			.getLogger(ChannelDao.class);

	@Override
	public Class<Channel> getEntityType() {
		return Channel.class;
	}

}
