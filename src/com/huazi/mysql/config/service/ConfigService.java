package com.huazi.mysql.config.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sshbase.util.CommonUtil;

import com.huazi.mysql.DaoManager;
import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseLesson;
import com.huazi.mysql.config.model.CoursePackage;
import com.huazi.mysql.config.model.CoursePackageLesson;

@Component
public class ConfigService {

	private static final Logger log = LoggerFactory
			.getLogger(ConfigService.class);
	

	@Autowired
	private DaoManager daoManager;


	public Document initCourse(Course course) {
		Document doc = new Document();
		
		String where = " where coursePackageId = " + course.getCoursePackageId() + " and enableFlag = 1 order by number asc "; 
		List<CoursePackageLesson> pageimpl = daoManager.getCoursePackageLessonDao().getListByWhere(where);

		int i = 0;
		for(CoursePackageLesson plesson : pageimpl){
			CourseLesson clesson = new CourseLesson();
			clesson.setCourseId(course.getId());
			clesson.setCoursePackageId(course.getCoursePackageId());
			clesson.setCoursePackageLessonId(plesson.getId());
			clesson.setName(plesson.getName());
			clesson.setTitle(plesson.getTitle());
			clesson.setKnowledges(plesson.getKnowledges());
			clesson.setNumber(plesson.getNumber());
			clesson.setVideoUrl(plesson.getVideoUrl());
			clesson.setDocUrl(plesson.getDocUrl());
			clesson.setCoverUrl(plesson.getCoverUrl());
			clesson.setTextbookUrl(plesson.getTextbookUrl());
			clesson.setShortKnowledges(plesson.getShortKnowledges());
			clesson.setSb3url(plesson.getSb3Url());
			clesson.setTimeMinute(30);
			clesson.setStartAt(CommonUtil.GetTimestampAddHour(CommonUtil.GetThisDayTimestamp(CommonUtil.GetNowTimestamp()), 24 * i + 19.5));
			clesson.setEndAt(CommonUtil.GetTimestampAddHour(clesson.getStartAt(), 0.5));
			clesson.setEnableFlag(1);
			daoManager.getCourseLessonDao().insert(clesson);
			i++;
		}
		return doc;
	}
	
	public Document resetPackageCourse(CoursePackage currentPackage) {
		
		String where = " where coursePackageId = " + currentPackage.getId() + " order by enableFlag asc,number asc "; 
		List<CoursePackageLesson> pageimpl = daoManager.getCoursePackageLessonDao().getListByWhere(where);

		List<CoursePackageLesson> lessonlist = new ArrayList<CoursePackageLesson>();
		if(currentPackage.getLessonCount() == null){
			currentPackage.setLessonCount(0);
		}
		boolean canEanble = true;
		for(int i = 0; i < pageimpl.size() ; i ++){
			CoursePackageLesson newlesson = pageimpl.get(i);
			if(i < currentPackage.getLessonCount()){
				if(newlesson.getState() == 0){
					canEanble = false;
				}
				if(newlesson.getEnableFlag() != 1){
					newlesson.setEnableFlag(1);
					daoManager.getCoursePackageLessonDao().update(newlesson);
				}
				lessonlist.add(newlesson);
			}
			else{
				if(newlesson.getEnableFlag() != 2){
					newlesson.setEnableFlag(2);
					daoManager.getCoursePackageLessonDao().update(newlesson);
				}
			}
		}
		if(currentPackage.getLessonCount() > pageimpl.size()){
			canEanble = false;
			for(int i = 0; i < currentPackage.getLessonCount() - pageimpl.size() ; i ++){
				CoursePackageLesson newlesson = new CoursePackageLesson();
				newlesson.setCoursePackageId(currentPackage.getId());
				newlesson.setNumber(pageimpl.size() + i + 1);
				newlesson.setName("课时" + newlesson.getNumber());
				newlesson.setEnableFlag(1);
				daoManager.getCoursePackageLessonDao().insert(newlesson);
				lessonlist.add(newlesson);
			}
		}
		
		if(canEanble == false && currentPackage.getState() == 1){
			currentPackage.setState(0);
			this.daoManager.getCoursePackageDao().update(currentPackage);
		}
		else if(canEanble == true && currentPackage.getState() == 0){
			currentPackage.setState(1);
			this.daoManager.getCoursePackageDao().update(currentPackage);
		}
		Document doc = currentPackage.document();
		doc.put("packageLessonInfo", lessonlist);
		return doc;
	}
	


}
