package com.huazi.mysql.admin.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.admin.model.Admin;

@Component
public class AdminDao extends BaseDao<Admin> {

	private static final Logger log = LoggerFactory
			.getLogger(AdminDao.class);

	@Override
	public Class<Admin> getEntityType() {
		return Admin.class;
	}

}
