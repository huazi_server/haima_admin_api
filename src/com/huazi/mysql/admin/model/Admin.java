package com.huazi.mysql.admin.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Admin entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "admin", catalog = "haimawang")
public class Admin extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long accountId;
	private String phone;
	private String email;
	private String name;
	private String avatarUrl;
	private String password;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public Admin() {
	}

	/** minimal constructor */
	public Admin(Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public Admin(Long accountId, String phone, String email, String name,
			String avatarUrl, String password, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.accountId = accountId;
		this.phone = phone;
		this.email = email;
		this.name = name;
		this.avatarUrl = avatarUrl;
		this.password = password;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "accountId")
	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Column(name = "phone", length = 20)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "email", length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "name", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "avatarUrl")
	public String getAvatarUrl() {
		return this.avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	@Column(name = "password", length = 50)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "state", nullable = false)
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", nullable = false, length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", nullable = false, length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}