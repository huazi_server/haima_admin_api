package com.huazi.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.huazi.mysql.config.service.ConfigService;
import com.huazi.mysql.student.service.StudentService;
import com.huazi.mysql.teacher.service.TeacherService;

@Component
public class MysqlManager extends DaoManager {

	@Autowired
	private StudentService studentService;
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private ConfigService configService;


	public ConfigService getConfigService() {
		return configService;
	}
	public StudentService getStudentService() {
		return studentService;
	}
	public TeacherService getTeacherService() {
		return teacherService;
	}
	
}
