package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Importing entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "importing", catalog = "haimawang")
public class Importing extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long coursePackageId;
	private Long courseId;
	private Long adminId;
	private Integer info;
	private String excelUrl;
	private String studentType;
	private Integer studentAmount;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public Importing() {
	}

	/** minimal constructor */
	public Importing(Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public Importing(Long coursePackageId, Long courseId, Long adminId,
			Integer info, String excelUrl, String studentType,
			Integer studentAmount, Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.coursePackageId = coursePackageId;
		this.courseId = courseId;
		this.adminId = adminId;
		this.info = info;
		this.excelUrl = excelUrl;
		this.studentType = studentType;
		this.studentAmount = studentAmount;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "coursePackageId")
	public Long getCoursePackageId() {
		return this.coursePackageId;
	}

	public void setCoursePackageId(Long coursePackageId) {
		this.coursePackageId = coursePackageId;
	}

	@Column(name = "courseId")
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "adminId")
	public Long getAdminId() {
		return this.adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	@Column(name = "info")
	public Integer getInfo() {
		return this.info;
	}

	public void setInfo(Integer info) {
		this.info = info;
	}

	@Column(name = "excelUrl")
	public String getExcelUrl() {
		return this.excelUrl;
	}

	public void setExcelUrl(String excelUrl) {
		this.excelUrl = excelUrl;
	}

	@Column(name = "studentType")
	public String getStudentType() {
		return this.studentType;
	}

	public void setStudentType(String studentType) {
		this.studentType = studentType;
	}

	@Column(name = "studentAmount")
	public Integer getStudentAmount() {
		return this.studentAmount;
	}

	public void setStudentAmount(Integer studentAmount) {
		this.studentAmount = studentAmount;
	}

	@Column(name = "state", nullable = false)
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", nullable = false, length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", nullable = false, length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}