package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * StudentCourse entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "student_course", catalog = "haimawang")
public class StudentCourse extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long studentId;
	private Long courseId;
	private Long coursePackageId;
	private Long teacherCourseId;
	private Long teacherId;
	private Integer score;
	private Integer sourceType;
	private Long sourceId;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public StudentCourse() {
	}

	/** minimal constructor */
	public StudentCourse(Long studentId, Long courseId, Integer delFlag) {
		this.studentId = studentId;
		this.courseId = courseId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public StudentCourse(Long studentId, Long courseId, Long coursePackageId,
			Long teacherCourseId, Long teacherId, Integer score,
			Integer sourceType, Long sourceId, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.studentId = studentId;
		this.courseId = courseId;
		this.coursePackageId = coursePackageId;
		this.teacherCourseId = teacherCourseId;
		this.teacherId = teacherId;
		this.score = score;
		this.sourceType = sourceType;
		this.sourceId = sourceId;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "studentId", nullable = false)
	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Column(name = "courseId", nullable = false)
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "coursePackageId")
	public Long getCoursePackageId() {
		return this.coursePackageId;
	}

	public void setCoursePackageId(Long coursePackageId) {
		this.coursePackageId = coursePackageId;
	}

	@Column(name = "teacherCourseId")
	public Long getTeacherCourseId() {
		return this.teacherCourseId;
	}

	public void setTeacherCourseId(Long teacherCourseId) {
		this.teacherCourseId = teacherCourseId;
	}

	@Column(name = "teacherId")
	public Long getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "sourceType")
	public Integer getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "sourceId")
	public Long getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}