package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * Student entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "student", catalog = "haimawang")
public class Student extends BaseEntity implements java.io.Serializable {

	// Fields

    private Long id;
    private Long accountId;
    private String code;
    private Integer type;
    private String name;
    private String nickName;
    private String wxNickName;
    private String openId;
    private String phone;
    private Integer sourceType;
    private Timestamp registerAt;
    private Integer enableFlag;
    private String avatarUrl;
    private String qrCodeUrl;
    private String email;
    private String info;
    private Integer gender;
    private String channelIds;
    private String remark;
    private Integer state;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Integer delFlag;


   // Constructors

   /** default constructor */
   public Student() {
   }

	/** minimal constructor */
   public Student(Integer delFlag) {
       this.delFlag = delFlag;
   }
   
   /** full constructor */
   public Student(Long accountId, String code, Integer type, String name, String nickName, String wxNickName, String openId, String phone, Integer sourceType, Timestamp registerAt, Integer enableFlag, String avatarUrl, String qrCodeUrl, String email, String info, Integer gender, String channelIds, String remark, Integer state, Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
       this.accountId = accountId;
       this.code = code;
       this.type = type;
       this.name = name;
       this.nickName = nickName;
       this.wxNickName = wxNickName;
       this.openId = openId;
       this.phone = phone;
       this.sourceType = sourceType;
       this.registerAt = registerAt;
       this.enableFlag = enableFlag;
       this.avatarUrl = avatarUrl;
       this.qrCodeUrl = qrCodeUrl;
       this.email = email;
       this.info = info;
       this.gender = gender;
       this.channelIds = channelIds;
       this.remark = remark;
       this.state = state;
       this.createdAt = createdAt;
       this.updatedAt = updatedAt;
       this.delFlag = delFlag;
   }

  
   // Property accessors
   @Id @GeneratedValue(strategy=IDENTITY)
   
   @Column(name="id", unique=true, nullable=false)

   public Long getId() {
       return this.id;
   }
   
   public void setId(Long id) {
       this.id = id;
   }
   
   @Column(name="accountId", unique=true)

   public Long getAccountId() {
       return this.accountId;
   }
   
   public void setAccountId(Long accountId) {
       this.accountId = accountId;
   }
   
   @Column(name="code", length=32)

   public String getCode() {
       return this.code;
   }
   
   public void setCode(String code) {
       this.code = code;
   }
   
   @Column(name="type")

   public Integer getType() {
       return this.type;
   }
   
   public void setType(Integer type) {
       this.type = type;
   }
   
   @Column(name="name", length=32)

   public String getName() {
       return this.name;
   }
   
   public void setName(String name) {
       this.name = name;
   }
   
   @Column(name="nickName", length=100)

   public String getNickName() {
       return this.nickName;
   }
   
   public void setNickName(String nickName) {
       this.nickName = nickName;
   }
   
   @Column(name="wxNickName", length=100)

   public String getWxNickName() {
       return this.wxNickName;
   }
   
   public void setWxNickName(String wxNickName) {
       this.wxNickName = wxNickName;
   }
   
   @Column(name="openId", length=1000)

   public String getOpenId() {
       return this.openId;
   }
   
   public void setOpenId(String openId) {
       this.openId = openId;
   }
   
   @Column(name="phone", length=32)

   public String getPhone() {
       return this.phone;
   }
   
   public void setPhone(String phone) {
       this.phone = phone;
   }
   
   @Column(name="sourceType")

   public Integer getSourceType() {
       return this.sourceType;
   }
   
   public void setSourceType(Integer sourceType) {
       this.sourceType = sourceType;
   }
   
   @Column(name="registerAt", length=19)

   public Timestamp getRegisterAt() {
       return this.registerAt;
   }
   
   public void setRegisterAt(Timestamp registerAt) {
       this.registerAt = registerAt;
   }
   
   @Column(name="enableFlag")

   public Integer getEnableFlag() {
       return this.enableFlag;
   }
   
   public void setEnableFlag(Integer enableFlag) {
       this.enableFlag = enableFlag;
   }
   
   @Column(name="avatarUrl", length=1000)

   public String getAvatarUrl() {
       return this.avatarUrl;
   }
   
   public void setAvatarUrl(String avatarUrl) {
       this.avatarUrl = avatarUrl;
   }
   
   @Column(name="qrCodeUrl", length=1000)

   public String getQrCodeUrl() {
       return this.qrCodeUrl;
   }
   
   public void setQrCodeUrl(String qrCodeUrl) {
       this.qrCodeUrl = qrCodeUrl;
   }
   
   @Column(name="email", length=1000)

   public String getEmail() {
       return this.email;
   }
   
   public void setEmail(String email) {
       this.email = email;
   }
   
   @Column(name="info", length=1000)

   public String getInfo() {
       return this.info;
   }
   
   public void setInfo(String info) {
       this.info = info;
   }
   
   @Column(name="gender")

   public Integer getGender() {
       return this.gender;
   }
   
   public void setGender(Integer gender) {
       this.gender = gender;
   }
   
   @Column(name="channelIds", length=1000)

   public String getChannelIds() {
       return this.channelIds;
   }
   
   public void setChannelIds(String channelIds) {
       this.channelIds = channelIds;
   }
   
   @Column(name="remark", length=1000)

   public String getRemark() {
       return this.remark;
   }
   
   public void setRemark(String remark) {
       this.remark = remark;
   }
   
   @Column(name="state")

   public Integer getState() {
       return this.state;
   }
   
   public void setState(Integer state) {
       this.state = state;
   }
   
   @Column(name="createdAt", length=19)

   public Timestamp getCreatedAt() {
       return this.createdAt;
   }
   
   public void setCreatedAt(Timestamp createdAt) {
       this.createdAt = createdAt;
   }
   
   @Column(name="updatedAt", length=19)

   public Timestamp getUpdatedAt() {
       return this.updatedAt;
   }
   
   public void setUpdatedAt(Timestamp updatedAt) {
       this.updatedAt = updatedAt;
   }
   
   @Column(name="delFlag", nullable=false)

   public Integer getDelFlag() {
       return this.delFlag;
   }
   
   public void setDelFlag(Integer delFlag) {
       this.delFlag = delFlag;
   }
}