package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * StudentCourseHomework entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "student_course_homework", catalog = "haimawang")
public class StudentCourseHomework extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long studentId;
	private Long courseId;
	private Long courseLessonId;
	private Long studentCourseId;
	private Long teacherId;
	private Long teacherCourseId;
	private Long teacherCourseLessonId;
	private Timestamp startAt;
	private Timestamp endAt;
	private String coverUrl;
	private String content;
	private String draft;
	private Integer answerSecond;
	private Timestamp commitAt;
	private Integer commitCount;
	private String comment;
	private Timestamp commentAt;
	private Timestamp onlineAt;
	private Integer onlineFlag;
	private Integer studyFlag;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public StudentCourseHomework() {
	}

	/** minimal constructor */
	public StudentCourseHomework(Long studentId, Long courseId,
			Long studentCourseId, Integer delFlag) {
		this.studentId = studentId;
		this.courseId = courseId;
		this.studentCourseId = studentCourseId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public StudentCourseHomework(Long studentId, Long courseId,
			Long courseLessonId, Long studentCourseId, Long teacherId,
			Long teacherCourseId, Long teacherCourseLessonId,
			Timestamp startAt, Timestamp endAt, String coverUrl,
			String content, String draft, Integer answerSecond,
			Timestamp commitAt, Integer commitCount, String comment,
			Timestamp commentAt, Timestamp onlineAt, Integer onlineFlag,
			Integer studyFlag, String remark, Integer state,
			Timestamp createdAt, Timestamp updatedAt, Integer delFlag) {
		this.studentId = studentId;
		this.courseId = courseId;
		this.courseLessonId = courseLessonId;
		this.studentCourseId = studentCourseId;
		this.teacherId = teacherId;
		this.teacherCourseId = teacherCourseId;
		this.teacherCourseLessonId = teacherCourseLessonId;
		this.startAt = startAt;
		this.endAt = endAt;
		this.coverUrl = coverUrl;
		this.content = content;
		this.draft = draft;
		this.answerSecond = answerSecond;
		this.commitAt = commitAt;
		this.commitCount = commitCount;
		this.comment = comment;
		this.commentAt = commentAt;
		this.onlineAt = onlineAt;
		this.onlineFlag = onlineFlag;
		this.studyFlag = studyFlag;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "studentId", nullable = false)
	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Column(name = "courseId", nullable = false)
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "courseLessonId")
	public Long getCourseLessonId() {
		return this.courseLessonId;
	}

	public void setCourseLessonId(Long courseLessonId) {
		this.courseLessonId = courseLessonId;
	}

	@Column(name = "studentCourseId", nullable = false)
	public Long getStudentCourseId() {
		return this.studentCourseId;
	}

	public void setStudentCourseId(Long studentCourseId) {
		this.studentCourseId = studentCourseId;
	}

	@Column(name = "teacherId")
	public Long getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Column(name = "teacherCourseId")
	public Long getTeacherCourseId() {
		return this.teacherCourseId;
	}

	public void setTeacherCourseId(Long teacherCourseId) {
		this.teacherCourseId = teacherCourseId;
	}

	@Column(name = "teacherCourseLessonId")
	public Long getTeacherCourseLessonId() {
		return this.teacherCourseLessonId;
	}

	public void setTeacherCourseLessonId(Long teacherCourseLessonId) {
		this.teacherCourseLessonId = teacherCourseLessonId;
	}

	@Column(name = "startAt", length = 19)
	public Timestamp getStartAt() {
		return this.startAt;
	}

	public void setStartAt(Timestamp startAt) {
		this.startAt = startAt;
	}

	@Column(name = "endAt", length = 19)
	public Timestamp getEndAt() {
		return this.endAt;
	}

	public void setEndAt(Timestamp endAt) {
		this.endAt = endAt;
	}

	@Column(name = "coverUrl", length = 1000)
	public String getCoverUrl() {
		return this.coverUrl;
	}

	public void setCoverUrl(String coverUrl) {
		this.coverUrl = coverUrl;
	}

	@Column(name = "content", length = 1000)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "draft", length = 1000)
	public String getDraft() {
		return this.draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	@Column(name = "answerSecond")
	public Integer getAnswerSecond() {
		return this.answerSecond;
	}

	public void setAnswerSecond(Integer answerSecond) {
		this.answerSecond = answerSecond;
	}

	@Column(name = "commitAt", length = 19)
	public Timestamp getCommitAt() {
		return this.commitAt;
	}

	public void setCommitAt(Timestamp commitAt) {
		this.commitAt = commitAt;
	}

	@Column(name = "commitCount")
	public Integer getCommitCount() {
		return this.commitCount;
	}

	public void setCommitCount(Integer commitCount) {
		this.commitCount = commitCount;
	}

	@Column(name = "comment", length = 1000)
	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Column(name = "commentAt", length = 19)
	public Timestamp getCommentAt() {
		return this.commentAt;
	}

	public void setCommentAt(Timestamp commentAt) {
		this.commentAt = commentAt;
	}

	@Column(name = "onlineAt", length = 19)
	public Timestamp getOnlineAt() {
		return this.onlineAt;
	}

	public void setOnlineAt(Timestamp onlineAt) {
		this.onlineAt = onlineAt;
	}

	@Column(name = "onlineFlag")
	public Integer getOnlineFlag() {
		return this.onlineFlag;
	}

	public void setOnlineFlag(Integer onlineFlag) {
		this.onlineFlag = onlineFlag;
	}

	@Column(name = "studyFlag")
	public Integer getStudyFlag() {
		return this.studyFlag;
	}

	public void setStudyFlag(Integer studyFlag) {
		this.studyFlag = studyFlag;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}