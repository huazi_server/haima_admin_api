package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * ImportingStudent entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "importing_student", catalog = "haimawang")
public class ImportingStudent extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long importingId;
	private Long userPhone;
	private String userName;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public ImportingStudent() {
	}

	/** minimal constructor */
	public ImportingStudent(Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public ImportingStudent(Long importingId, Long userPhone, String userName,
			String remark, Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.importingId = importingId;
		this.userPhone = userPhone;
		this.userName = userName;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "importingId")
	public Long getImportingId() {
		return this.importingId;
	}

	public void setImportingId(Long importingId) {
		this.importingId = importingId;
	}

	@Column(name = "userPhone")
	public Long getUserPhone() {
		return this.userPhone;
	}

	public void setUserPhone(Long userPhone) {
		this.userPhone = userPhone;
	}

	@Column(name = "userName", length = 100)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state", nullable = false)
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", nullable = false, length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", nullable = false, length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}