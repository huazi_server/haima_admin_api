package com.huazi.mysql.student.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * StudentCourseOrder entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "student_course_order", catalog = "haimawang")
public class StudentCourseOrder extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long studentId;
	private String phone;
	private Long channelId;
	private Integer courseType;
	private Long courseGroupId;
	private String coursePackageIds;
	private String courseIds;
	private String orderNo;
	private Integer sourceType;
	private Double orderMoney;
	private Double payMoney;
	private Double refundMoney;
	private Timestamp payAt;
	private Integer payChannel;
	private String payType;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public StudentCourseOrder() {
	}

	/** minimal constructor */
	public StudentCourseOrder(Long studentId, String coursePackageIds,
			String courseIds, String orderNo, Integer sourceType,
			Integer delFlag) {
		this.studentId = studentId;
		this.coursePackageIds = coursePackageIds;
		this.courseIds = courseIds;
		this.orderNo = orderNo;
		this.sourceType = sourceType;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public StudentCourseOrder(Long studentId, String phone, Long channelId,
			Integer courseType, Long courseGroupId, String coursePackageIds,
			String courseIds, String orderNo, Integer sourceType,
			Double orderMoney, Double payMoney, Double refundMoney,
			Timestamp payAt, Integer payChannel, String payType, String remark,
			Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.studentId = studentId;
		this.phone = phone;
		this.channelId = channelId;
		this.courseType = courseType;
		this.courseGroupId = courseGroupId;
		this.coursePackageIds = coursePackageIds;
		this.courseIds = courseIds;
		this.orderNo = orderNo;
		this.sourceType = sourceType;
		this.orderMoney = orderMoney;
		this.payMoney = payMoney;
		this.refundMoney = refundMoney;
		this.payAt = payAt;
		this.payChannel = payChannel;
		this.payType = payType;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "studentId", nullable = false)
	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	@Column(name = "phone", length = 32)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "channelId")
	public Long getChannelId() {
		return this.channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "courseType")
	public Integer getCourseType() {
		return this.courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	@Column(name = "courseGroupId")
	public Long getCourseGroupId() {
		return this.courseGroupId;
	}

	public void setCourseGroupId(Long courseGroupId) {
		this.courseGroupId = courseGroupId;
	}

	@Column(name = "coursePackageIds", nullable = false, length = 1000)
	public String getCoursePackageIds() {
		return this.coursePackageIds;
	}

	public void setCoursePackageIds(String coursePackageIds) {
		this.coursePackageIds = coursePackageIds;
	}

	@Column(name = "courseIds", nullable = false, length = 1000)
	public String getCourseIds() {
		return this.courseIds;
	}

	public void setCourseIds(String courseIds) {
		this.courseIds = courseIds;
	}

	@Column(name = "orderNo", nullable = false, length = 20)
	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name = "sourceType", nullable = false)
	public Integer getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "orderMoney", precision = 10)
	public Double getOrderMoney() {
		return this.orderMoney;
	}

	public void setOrderMoney(Double orderMoney) {
		this.orderMoney = orderMoney;
	}

	@Column(name = "payMoney", precision = 10)
	public Double getPayMoney() {
		return this.payMoney;
	}

	public void setPayMoney(Double payMoney) {
		this.payMoney = payMoney;
	}

	@Column(name = "refundMoney", precision = 10)
	public Double getRefundMoney() {
		return this.refundMoney;
	}

	public void setRefundMoney(Double refundMoney) {
		this.refundMoney = refundMoney;
	}

	@Column(name = "payAt", length = 19)
	public Timestamp getPayAt() {
		return this.payAt;
	}

	public void setPayAt(Timestamp payAt) {
		this.payAt = payAt;
	}

	@Column(name = "payChannel")
	public Integer getPayChannel() {
		return this.payChannel;
	}

	public void setPayChannel(Integer payChannel) {
		this.payChannel = payChannel;
	}

	@Column(name = "payType", length = 32)
	public String getPayType() {
		return this.payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}