package com.huazi.mysql.student.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.DaoManager;
import com.huazi.mysql.config.model.CourseLesson;
import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.student.model.StudentCourseOrder;
import com.huazi.mysql.teacher.model.TeacherCourse;
import com.huazi.mysql.teacher.model.TeacherCourseLesson;

@Component
public class StudentService {

	private static final Logger log = LoggerFactory
			.getLogger(StudentService.class);
	

	@Autowired
	private DaoManager daoManager;
	

	public StudentCourseOrder refundOrder(StudentCourseOrder order) {
		
		
		return order;
	}
}
