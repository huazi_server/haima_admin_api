package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.ImportingStudent;


@Component
public class ImportingStudentDao extends BaseDao<ImportingStudent> {

	private static final Logger log = LoggerFactory
			.getLogger(ImportingStudentDao.class);

	@Override
	public Class<ImportingStudent> getEntityType() {
		return ImportingStudent.class;
	}

}
