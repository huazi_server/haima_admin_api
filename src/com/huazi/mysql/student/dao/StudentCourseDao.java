package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.StudentCourse;


@Component
public class StudentCourseDao extends BaseDao<StudentCourse> {

	private static final Logger log = LoggerFactory
			.getLogger(StudentCourseOrderDao.class);

	@Override
	public Class<StudentCourse> getEntityType() {
		return StudentCourse.class;
	}

}
