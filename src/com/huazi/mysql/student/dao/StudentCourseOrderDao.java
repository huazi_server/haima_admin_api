package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.StudentCourseOrder;


@Component
public class StudentCourseOrderDao extends BaseDao<StudentCourseOrder> {

	private static final Logger log = LoggerFactory
			.getLogger(StudentCourseOrderDao.class);

	@Override
	public Class<StudentCourseOrder> getEntityType() {
		return StudentCourseOrder.class;
	}

}
