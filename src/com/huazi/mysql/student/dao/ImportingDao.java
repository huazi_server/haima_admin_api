package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.Importing;


@Component
public class ImportingDao extends BaseDao<Importing> {

	private static final Logger log = LoggerFactory
			.getLogger(ImportingDao.class);

	@Override
	public Class<Importing> getEntityType() {
		return Importing.class;
	}

}
