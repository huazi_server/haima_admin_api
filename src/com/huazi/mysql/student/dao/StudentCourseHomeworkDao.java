package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.StudentCourseHomework;


@Component
public class StudentCourseHomeworkDao extends BaseDao<StudentCourseHomework> {

	private static final Logger log = LoggerFactory
			.getLogger(StudentCourseHomeworkDao.class);

	@Override
	public Class<StudentCourseHomework> getEntityType() {
		return StudentCourseHomework.class;
	}

}
