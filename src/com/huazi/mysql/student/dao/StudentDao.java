package com.huazi.mysql.student.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.student.model.Student;


@Component
public class StudentDao extends BaseDao<Student> {

	private static final Logger log = LoggerFactory
			.getLogger(StudentDao.class);

	@Override
	public Class<Student> getEntityType() {
		return Student.class;
	}

}
