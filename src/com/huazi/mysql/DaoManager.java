package com.huazi.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.huazi.mysql.admin.dao.AdminDao;
import com.huazi.mysql.config.dao.ActivityFreeDao;
import com.huazi.mysql.config.dao.ActivityTuanDao;
import com.huazi.mysql.config.dao.ChannelActivityDao;
import com.huazi.mysql.config.dao.ChannelDao;
import com.huazi.mysql.config.dao.ChannelStatisticsDao;
import com.huazi.mysql.config.dao.CommentDao;
import com.huazi.mysql.config.dao.CourseDao;
import com.huazi.mysql.config.dao.CourseGroupDao;
import com.huazi.mysql.config.dao.CourseLessonDao;
import com.huazi.mysql.config.dao.CoursePackageDao;
import com.huazi.mysql.config.dao.CoursePackageLessonDao;
import com.huazi.mysql.config.dao.CoursePackageLessonVideoPointDao;
import com.huazi.mysql.config.dao.ProductionCommentDao;
import com.huazi.mysql.config.dao.ProductionCommentReplyDao;
import com.huazi.mysql.config.dao.ProductionDao;
import com.huazi.mysql.config.dao.ReportConfigDao;
import com.huazi.mysql.config.dao.ReportDao;
import com.huazi.mysql.student.dao.ImportingDao;
import com.huazi.mysql.student.dao.ImportingStudentDao;
import com.huazi.mysql.student.dao.StudentCourseDao;
import com.huazi.mysql.student.dao.StudentCourseHomeworkDao;
import com.huazi.mysql.student.dao.StudentCourseOrderDao;
import com.huazi.mysql.student.dao.StudentDao;
import com.huazi.mysql.teacher.dao.TeacherCourseDao;
import com.huazi.mysql.teacher.dao.TeacherCourseLessonDao;
import com.huazi.mysql.teacher.dao.TeacherDao;

@Component
public class DaoManager{

	@Autowired
	private AdminDao adminDao;
	public AdminDao getAdminDao() {
		return adminDao;
	}

	@Autowired
	private ImportingDao importingDao;
	public ImportingDao getImportingDao() {
		return importingDao;
	}
	@Autowired
	private ImportingStudentDao importingStudentDao;
	public ImportingStudentDao getImportingStudentDao() {
		return importingStudentDao;
	}
	
	@Autowired
	private ReportDao reportDao;
	public ReportDao getReportDao() {
		return reportDao;
	}
	
	@Autowired
	private ReportConfigDao reportConfigDao;
	public ReportConfigDao getReportConfigDao() {
		return reportConfigDao;
	}
	
	
	
	@Autowired
	private CommentDao commentDao;
	public CommentDao getCommentDao() {
		return commentDao;
	}

	@Autowired
	private ProductionDao productionDao;
	public ProductionDao getProductionDao() {
		return productionDao;
	}
	@Autowired
	private ProductionCommentDao productionCommentDao;
	public ProductionCommentDao getProductionCommentDao() {
		return productionCommentDao;
	}
	@Autowired
	private ProductionCommentReplyDao productionCommentReplyDao;
	public ProductionCommentReplyDao getProductionCommentReplyDao() {
		return productionCommentReplyDao;
	}

	@Autowired
	private ActivityFreeDao activityFreeDao;
	public ActivityFreeDao getActivityFreeDao() {
		return activityFreeDao;
	}
	@Autowired
	private ActivityTuanDao activityTuanDao;
	public ActivityTuanDao getActivityTuanDao() {
		return activityTuanDao;
	}
	
	
	@Autowired
	private ChannelDao channelDao;
	public ChannelDao getChannelDao() {
		return channelDao;
	}
	@Autowired
	private ChannelActivityDao channelActivityDao;
	public ChannelActivityDao getChannelActivityDao() {
		return channelActivityDao;
	}
	@Autowired
	private ChannelStatisticsDao channelStatisticsDao;
	public ChannelStatisticsDao getChannelStatisticsDao() {
		return channelStatisticsDao;
	}
	
	@Autowired
	private TeacherDao teacherDao;
	public TeacherDao getTeacherDao() {
		return teacherDao;
	}
	@Autowired
	private TeacherCourseDao teacherCourseDao;
	public TeacherCourseDao getTeacherCourseDao() {
		return teacherCourseDao;
	}
	@Autowired
	private TeacherCourseLessonDao teacherCourseLessonDao;
	public TeacherCourseLessonDao getTeacherCourseLessonDao() {
		return teacherCourseLessonDao;
	}


	@Autowired
	private StudentDao studentDao;
	public StudentDao getStudentDao() {
		return studentDao;
	}
	@Autowired
	private StudentCourseDao StudentCourseDao;
	public StudentCourseDao getStudentCourseDao() {
		return StudentCourseDao;
	}
	@Autowired
	private StudentCourseOrderDao studentCourseOrderDao;
	public StudentCourseOrderDao getStudentCourseOrderDao() {
		return studentCourseOrderDao;
	}
	@Autowired
	private StudentCourseHomeworkDao studentCourseHomeworkDao;
	public StudentCourseHomeworkDao getStudentCourseHomeworkDao() {
		return studentCourseHomeworkDao;
	}
	

	@Autowired
	private CourseDao courseDao;
	public CourseDao getCourseDao() {
		return courseDao;
	}
	@Autowired
	private CourseLessonDao courseLessonDao;
	public CourseLessonDao getCourseLessonDao() {
		return courseLessonDao;
	}
	@Autowired
	private CourseGroupDao courseGroupDao;
	public CourseGroupDao getCourseGroupDao() {
		return courseGroupDao;
	}
	@Autowired
	private CoursePackageDao coursePackageDao;
	public CoursePackageDao getCoursePackageDao() {
		return coursePackageDao;
	}
	@Autowired
	private CoursePackageLessonDao coursePackageLessonDao;
	public CoursePackageLessonDao getCoursePackageLessonDao() {
		return coursePackageLessonDao;
	}
	@Autowired
	private CoursePackageLessonVideoPointDao coursePackageLessonVideoPointDao;
	public CoursePackageLessonVideoPointDao getCoursePackageLessonVideoPointDao() {
		return coursePackageLessonVideoPointDao;
	}
}
