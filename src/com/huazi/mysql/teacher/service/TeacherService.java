package com.huazi.mysql.teacher.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sshbase.util.CommonUtil;

import com.huazi.mysql.DaoManager;
import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseLesson;
import com.huazi.mysql.config.model.CoursePackageLesson;
import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.student.model.StudentCourseHomework;
import com.huazi.mysql.teacher.model.Teacher;
import com.huazi.mysql.teacher.model.TeacherCourse;
import com.huazi.mysql.teacher.model.TeacherCourseLesson;

@Component
public class TeacherService {

	private static final Logger log = LoggerFactory
			.getLogger(TeacherService.class);
	
    
	@Autowired
	private DaoManager daoManager;

	// TODO 需要优化

	public void coursePlanTeachers(Course course) {

		List<Long> ids = CommonUtil.GetLongList(course.getPlanTeacherIds());
		List<TeacherCourse> list = this.daoManager.getTeacherCourseDao().getListByWhere(" where courseId = " + course.getId());
		List<Long> deleteids = new ArrayList<Long>();
		boolean hasone = false;
		for(TeacherCourse tc : list){
			hasone = false;
			for(Long id : ids){
				if(tc.getTeacherId().longValue() == id.longValue()){
					hasone = true;
					if(tc.getDelFlag() == 1){
						tc.setDelFlag(0);
						tc.setScore(0);
						tc.setStudentCount(0);
						this.daoManager.getTeacherCourseDao().update(tc);
					}
					break;
				}
			}
			if(hasone == false){
				tc.setDelFlag(1);
				tc.setScore(0);
				tc.setStudentCount(0);
				deleteids.add(tc.getTeacherId());
				this.daoManager.getTeacherCourseDao().update(tc);
			}
		}
		if(deleteids.size() > 0){
			this.daoManager.getStudentCourseDao().execute(" update student_course set state = 1 ,teacherId = NULL,teacherCourseId = NULL "
					+ "where courseId = " + course.getId() + " and teacherId in (" + CommonUtil.GetStringList(deleteids)+ ") ");
		}

		for(Long id : ids){
			hasone = false;
			for(TeacherCourse tc : list){
				if(tc.getTeacherId().longValue() == id.longValue()){
					hasone = true;
					break;
				}
			}
			if(hasone == false){
				TeacherCourse tc = new TeacherCourse();
				tc.setCourseId(course.getId());
				tc.setTeacherId(id);
				tc.setScore(0);
				tc.setStudentCount(0);
				this.daoManager.getTeacherCourseDao().insert(tc);
			}
		}
		
	}
	public StudentCourse SettleStudentCourse(StudentCourse studentcourse,TeacherCourse teachercourse) {
		
		if(studentcourse.getTeacherId() != null && teachercourse.getTeacherId().longValue() == studentcourse.getTeacherId()){
			return studentcourse;
		}
		
		if(studentcourse.getTeacherId() != null && studentcourse.getTeacherCourseId() != null){
			TeacherCourse tc =  this.daoManager.getTeacherCourseDao().getById(studentcourse.getTeacherCourseId());
			if(tc != null){
				if(tc.getStudentCount() == null){
					tc.setStudentCount(0);
				}
				tc.setStudentCount(tc.getStudentCount() - 1);
				if(tc.getStudentCount() < 0){
					tc.setStudentCount(0);
				}
				this.daoManager.getTeacherCourseDao().update(tc);
			}
		}
		
		studentcourse.setTeacherId(teachercourse.getTeacherId());
		studentcourse.setTeacherCourseId(teachercourse.getId());
		studentcourse.setState(2);
		this.daoManager.getStudentCourseDao().update(studentcourse);
		
		if(teachercourse.getStudentCount() == null){
			teachercourse.setStudentCount(0);
		}
		teachercourse.setStudentCount(teachercourse.getStudentCount() + 1);
		this.daoManager.getTeacherCourseDao().update(teachercourse);
		
		return studentcourse;
	}


}
