package com.huazi.mysql.teacher.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * TeacherCourse entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "teacher_course", catalog = "haimawang")
public class TeacherCourse extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long teacherId;
	private Long courseId;
	private Integer studentCount;
	private Integer score;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public TeacherCourse() {
	}

	/** minimal constructor */
	public TeacherCourse(Long teacherId, Long courseId, Integer delFlag) {
		this.teacherId = teacherId;
		this.courseId = courseId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public TeacherCourse(Long teacherId, Long courseId, Integer studentCount,
			Integer score, String remark, Integer state, Timestamp createdAt,
			Timestamp updatedAt, Integer delFlag) {
		this.teacherId = teacherId;
		this.courseId = courseId;
		this.studentCount = studentCount;
		this.score = score;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "teacherId", nullable = false)
	public Long getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Column(name = "courseId", nullable = false)
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "studentCount")
	public Integer getStudentCount() {
		return this.studentCount;
	}

	public void setStudentCount(Integer studentCount) {
		this.studentCount = studentCount;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}