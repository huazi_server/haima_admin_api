package com.huazi.mysql.teacher.model;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

import sshbase.mysql.BaseEntity;

/**
 * TeacherCourseLesson entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "teacher_course_lesson", catalog = "haimawang")
public class TeacherCourseLesson extends BaseEntity implements java.io.Serializable {

	// Fields

	private Long id;
	private Long teacherId;
	private Long courseId;
	private Long courseLessonId;
	private Long teacherCourseId;
	private Integer allCount;
	private Integer commitCount;
	private Integer onlineCount;
	private String remark;
	private Integer state;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Integer delFlag;

	// Constructors

	/** default constructor */
	public TeacherCourseLesson() {
	}

	/** minimal constructor */
	public TeacherCourseLesson(Long teacherId, Long courseId,
			Long courseLessonId, Integer delFlag) {
		this.teacherId = teacherId;
		this.courseId = courseId;
		this.courseLessonId = courseLessonId;
		this.delFlag = delFlag;
	}

	/** full constructor */
	public TeacherCourseLesson(Long teacherId, Long courseId,
			Long courseLessonId, Long teacherCourseId, Integer allCount,
			Integer commitCount, Integer onlineCount, String remark,
			Integer state, Timestamp createdAt, Timestamp updatedAt,
			Integer delFlag) {
		this.teacherId = teacherId;
		this.courseId = courseId;
		this.courseLessonId = courseLessonId;
		this.teacherCourseId = teacherCourseId;
		this.allCount = allCount;
		this.commitCount = commitCount;
		this.onlineCount = onlineCount;
		this.remark = remark;
		this.state = state;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "teacherId", nullable = false)
	public Long getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Column(name = "courseId", nullable = false)
	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	@Column(name = "courseLessonId", nullable = false)
	public Long getCourseLessonId() {
		return this.courseLessonId;
	}

	public void setCourseLessonId(Long courseLessonId) {
		this.courseLessonId = courseLessonId;
	}

	@Column(name = "teacherCourseId")
	public Long getTeacherCourseId() {
		return this.teacherCourseId;
	}

	public void setTeacherCourseId(Long teacherCourseId) {
		this.teacherCourseId = teacherCourseId;
	}

	@Column(name = "allCount")
	public Integer getAllCount() {
		return this.allCount;
	}

	public void setAllCount(Integer allCount) {
		this.allCount = allCount;
	}

	@Column(name = "commitCount")
	public Integer getCommitCount() {
		return this.commitCount;
	}

	public void setCommitCount(Integer commitCount) {
		this.commitCount = commitCount;
	}

	@Column(name = "onlineCount")
	public Integer getOnlineCount() {
		return this.onlineCount;
	}

	public void setOnlineCount(Integer onlineCount) {
		this.onlineCount = onlineCount;
	}

	@Column(name = "remark", length = 1000)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "createdAt", length = 19)
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updatedAt", length = 19)
	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "delFlag", nullable = false)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}