package com.huazi.mysql.teacher.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.teacher.model.TeacherCourseLesson;


@Component
public class TeacherCourseLessonDao extends BaseDao<TeacherCourseLesson> {

	private static final Logger log = LoggerFactory
			.getLogger(TeacherCourseLessonDao.class);

	@Override
	public Class<TeacherCourseLesson> getEntityType() {
		return TeacherCourseLesson.class;
	}

}
