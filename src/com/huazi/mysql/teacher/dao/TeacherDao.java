package com.huazi.mysql.teacher.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.teacher.model.Teacher;


@Component
public class TeacherDao extends BaseDao<Teacher> {

	private static final Logger log = LoggerFactory
			.getLogger(TeacherDao.class);

	@Override
	public Class<Teacher> getEntityType() {
		return Teacher.class;
	}

}
