package com.huazi.mysql.teacher.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sshbase.mysql.BaseDao;

import com.huazi.mysql.teacher.model.TeacherCourse;


@Component
public class TeacherCourseDao extends BaseDao<TeacherCourse> {

	private static final Logger log = LoggerFactory
			.getLogger(TeacherCourseDao.class);

	@Override
	public Class<TeacherCourse> getEntityType() {
		return TeacherCourse.class;
	}

}
