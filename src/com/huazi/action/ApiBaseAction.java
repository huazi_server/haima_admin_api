package com.huazi.action;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import s3part.service.AccountService;
import sshbase.struts2.action.BaseAction;
import sshbase.struts2.result.ApiResultObject;

import com.alibaba.fastjson.JSONObject;
import com.huazi.mysql.MysqlManager;
import com.huazi.mysql.student.model.Student;

public class ApiBaseAction<T> extends BaseAction<T>{


	@Autowired
	protected MysqlManager mysqlMgr;
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ApiBaseAction.class);

	public ApiBaseAction() {

	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	
	private Long loginId;
	private String loginToken;
	public Long getLoginId() {
		return loginId;
	}

	public void setLoginId(Long loginId) {
		this.loginId = loginId;
	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public JSONObject loginAccountLogin;
	public JSONObject loginAccount;
	public boolean valijectLoginAccount() {
		log.debug("");

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getLoginId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setLoginId(aId);
			}
			valijectMethod = this.getModel().getClass().getMethod("getLoginToken", new Class[0]);
			if(valijectMethod!=null){
				String aId = (String) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setLoginToken(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 
		
		loginAccountLogin = null;
		loginAccount = null;

		apiResultObject.setFailure(ApiResultObject.ValijectInput, "输入参数有误");
		
		this.apiResultObject.set(AccountService.getLogin(null, this.getLoginToken()));

		
		if(apiResultObject.getResult() == true){
			loginAccount = (JSONObject)apiResultObject.getData();
		}
		else{
			apiResultObject.setFailure(ApiResultObject.NotLogin, "token失效或暂未登录");
		}

		return apiResultObject.getResult();
	}
}