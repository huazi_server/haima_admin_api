package com.huazi.action.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huazi.action.ApiBaseAction;

public class AccountBaseAction<T> extends ApiBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AccountBaseAction.class);

	public AccountBaseAction() {
		log.debug("");
	}
	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}



}
