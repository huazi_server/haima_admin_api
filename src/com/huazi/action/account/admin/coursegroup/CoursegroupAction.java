package com.huazi.action.account.admin.coursegroup;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.config.model.CourseGroup;

public class CoursegroupAction extends CoursegroupBaseAction<CourseGroup>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CoursegroupAction.class);

	public CoursegroupAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getModel().getState() != null && this.getModel().getState() != -1){
			if(this.getModel().getState() == 1){
				where = "where state <= " + this.getModel().getState() +" ";
			}
			else{
				where = "where state = " + this.getModel().getState() +" ";
			}
		} 
		
		where += "order by id desc ";
		
		PageResultObject<CourseGroup> pageimpl = mysqlMgr.getCourseGroupDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");

		this.getModel().setCategory(1);
		this.getModel().setEnableFlag(0);
		this.getModel().setCourseGroupFlag(1);
		this.getModel().setState(1);
		currentGroup =  mysqlMgr.getCourseGroupDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(currentGroup);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentGroup")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(this.currentGroup);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentGroup")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentGroup.setState(this.getModel().getState());
			if(currentGroup.getState() <= 1 )currentGroup.setEnableFlag(0);
			if(currentGroup.getState() == 2 )currentGroup.setEnableFlag(1);
			if(currentGroup.getState() == 3 )currentGroup.setEnableFlag(0);
		}		
		if(this.getModel().getName() != null){
			currentGroup.setName(this.getModel().getName());
		}
		if(this.getModel().getIssue() != null){
			currentGroup.setIssue(this.getModel().getIssue());
		}
		if(this.getModel().getRemark() != null){
			currentGroup.setRemark(this.getModel().getRemark());
		}
		if(this.getModel().getPrice() != null){
			currentGroup.setPrice(this.getModel().getPrice());
		}
		if(this.getModel().getRealPrice() != null){ 
			currentGroup.setRealPrice(this.getModel().getRealPrice());
		}
		mysqlMgr.getCourseGroupDao().update(currentGroup);
		
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentGroup")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentGroup);
		return SUCCESS;
	}
}
