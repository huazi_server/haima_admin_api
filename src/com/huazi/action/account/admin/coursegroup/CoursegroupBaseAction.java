package com.huazi.action.account.admin.coursegroup;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.CourseGroup;

public class CoursegroupBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CoursegroupBaseAction.class);

	public CoursegroupBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}

}
