package com.huazi.action.account.admin.course.teacher;

import java.util.ArrayList;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.teacher.model.Teacher;
import com.huazi.mysql.teacher.model.TeacherCourse;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;

public class TeacherAction extends TeacherBaseAction<TeacherCourse>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeacherAction.class);

	public TeacherAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = " where courseId = " + this.currentCourse.getId() + " and delFlag = 0 ";


		where += "order by id desc ";
		
		PageResultObject<TeacherCourse> pageimpl = mysqlMgr.getTeacherCourseDao().getPageByWhere(pageable, where);
		
		
		docResult.put("pageable",pageimpl.getPageable());
		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		ArrayList<Long> teacherids = new ArrayList<Long>();
		for(TeacherCourse live : pageimpl.getContent()){
			if(live.getTeacherId()!=null)teacherids.add(live.getTeacherId());
		}
		Map<String,Teacher> teachermap  = this.mysqlMgr.getTeacherDao().getMapByIds(teacherids);
		
		
		for(TeacherCourse live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getTeacherId() != null){
				Teacher mcs = teachermap.get(live.getTeacherId().toString());
				if(mcs != null)livemap.put("teacherInfo", mcs.documentBrief());
			}
			pusherlist.add(livemap);
		}
		this.apiResultObject.setSuccess(docResult);
		return SUCCESS;
	}

	
	@Validations(
		requiredFields={
			@RequiredFieldValidator(fieldName="phone",message="电话号码"),
			@RequiredFieldValidator(fieldName="name",message="姓名"),
		}
	)
	@ValijectList(list = { @Valiject("loginAdmin"), @Valiject("currentCourse"), @Valiject("currentTeacher")})	
	public String create(){
		log.debug("");
		TeacherCourse model = new TeacherCourse();
		model.setTeacherId(currentTeacher.getId());
		model.setCourseId(currentCourse.getId());
		TeacherCourse teacher = mysqlMgr.getTeacherCourseDao().getByExample(model);
		if(teacher == null){
			model.setScore(0);
			model.setStudentCount(0);
			mysqlMgr.getTeacherCourseDao().insert(model);
			this.apiResultObject.setSuccess(model);
			return SUCCESS;
		}
		else if(teacher.getState() == 1){
			teacher.setState(0);
			model.setScore(0);
			model.setStudentCount(0);
			mysqlMgr.getTeacherCourseDao().update(teacher);
			this.apiResultObject.setSuccess(teacher);
			return SUCCESS;
		}
		else{
			this.apiResultObject.setFailure(ApiResultObject.HasExist, "教师已经存在");
			return SUCCESS;
		}
	}
	
	@ValijectList(list = { @Valiject("currentTeacherCourse")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentTeacherCourse);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentTeacherCourse")})
	public String update(){
		log.debug(""); 

		if(this.getModel().getState() != null){
			currentTeacherCourse.setState(this.getModel().getState());
		}

		mysqlMgr.getTeacherCourseDao().update(currentTeacherCourse);
		
		this.apiResultObject.setSuccess(currentTeacherCourse);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentTeacherCourse")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentTeacherCourse);
		return SUCCESS;
	}
}
