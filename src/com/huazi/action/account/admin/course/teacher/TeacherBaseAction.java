package com.huazi.action.account.admin.course.teacher;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.action.account.admin.course.CourseBaseAction;
import com.huazi.mysql.teacher.model.Teacher;
import com.huazi.mysql.teacher.model.TeacherCourse;

public class TeacherBaseAction<T> extends CourseBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeacherBaseAction.class);

	public TeacherBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}

	public TeacherCourse currentTeacherCourse;

	public boolean valijectCurrentTeacherCourse() {
		log.debug("");

		if(this.valijectCurrentCourse() == false){
			return apiResultObject.getResult();
		}
		if(this.valijectCurrentTeacher() == false){
			return apiResultObject.getResult();
		}

		TeacherCourse scmodel = new TeacherCourse();
		scmodel.setCourseId(this.currentCourse.getId());
		scmodel.setTeacherId(this.currentTeacher.getId());
		scmodel.setDelFlag(0);
		currentTeacherCourse = this.mysqlMgr.getTeacherCourseDao().getByExample(scmodel);

		if (currentTeacherCourse == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "教师课程不存在");
			return apiResultObject.getResult();
		}
		
		apiResultObject.setSuccess("Teacher Valiject Success");
		return apiResultObject.getResult();
	}


}
