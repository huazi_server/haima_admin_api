package com.huazi.action.account.admin.course.importing;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.action.account.admin.course.CourseBaseAction;
import com.huazi.mysql.student.model.Importing;

public class ImportingBaseAction<T> extends CourseBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ImportingBaseAction.class);

	public ImportingBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}


	public Importing currentImporting;
	private Long courseId;
	public Long getImportingId() {
		return courseId;
	}
	public void setImportingId(Long courseId) {
		this.courseId = courseId;
	}
	public boolean valijectCurrentImporting() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getImportingId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setImportingId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getImportingId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Importing");
			return apiResultObject.getResult();
		}

		long aid = this.getImportingId();
		Importing model = mysqlMgr.getImportingDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Importing");
			return apiResultObject.getResult();
		}
		currentImporting = model;
		apiResultObject.setSuccess("Importing Valiject Success");
		return apiResultObject.getResult();
	}

}
