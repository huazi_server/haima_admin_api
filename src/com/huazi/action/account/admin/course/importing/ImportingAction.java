package com.huazi.action.account.admin.course.importing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.huazi.mysql.student.model.Importing;
import com.huazi.mysql.teacher.model.Teacher;

public class ImportingAction extends ImportingBaseAction<Importing>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ImportingAction.class);

	public ImportingAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}
		
		where += "order by id desc ";
		
		PageResultObject<Importing> pageimpl = mysqlMgr.getImportingDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin"),@Valiject("currentCourse")})
	public String create(){
		log.debug("");
		Importing model = new Importing();
		model.setExcelUrl(this.getModel().getExcelUrl());
		Importing importing = mysqlMgr.getImportingDao().getByExample(model);
		
		if(importing != null){
			this.apiResultObject.setFailure(ApiResultObject.HasExist, "此文件已存在");
			return SUCCESS;
		}
		model.setCourseId(this.currentCourse.getId());
		model.setCoursePackageId(this.currentCourse.getCoursePackageId());
		model.setInfo(this.getModel().getInfo());
		model.setAdminId(loginAdmin.getId());
		
		this.apiResultObject.setSuccess(importing);
		return SUCCESS;
		
	}
	
	@ValijectList(list = { @Valiject("currentImporting")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentImporting);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentImporting")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getInfo() != null){
			currentImporting.setInfo(this.getModel().getInfo());
		}
		if(this.getModel().getState() != null){
			currentImporting.setState(this.getModel().getState());
		}
		
		mysqlMgr.getImportingDao().update(currentImporting);
		this.apiResultObject.setSuccess(currentImporting);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentImporting")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentImporting);
		return SUCCESS;
	}
}
