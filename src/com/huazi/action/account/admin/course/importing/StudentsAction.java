package com.huazi.action.account.admin.course.importing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.huazi.mysql.student.model.ImportingStudent;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.teacher.model.Teacher;

public class StudentsAction extends ImportingBaseAction<ImportingStudent>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(StudentsAction.class);

	public StudentsAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}
		
		if(this.getSearchString() != null && CommonUtil.GetStringLong(this.getSearchString()) != null){
			where += " and studentId = " + this.getSearchString() +" ";
		}

		where += "order by id desc ";
		
		PageResultObject<Student> pageimpl = mysqlMgr.getStudentDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

}
