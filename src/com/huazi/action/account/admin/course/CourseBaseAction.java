package com.huazi.action.account.admin.course;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.Course;

public class CourseBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CourseBaseAction.class);

	public CourseBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public Course currentCourse;
	private Long courseId;
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public boolean valijectCurrentCourse() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getCourseId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setCourseId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getCourseId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Course");
			return apiResultObject.getResult();
		}

		long aid = this.getCourseId();
		Course model = mysqlMgr.getCourseDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Course");
			return apiResultObject.getResult();
		}
		currentCourse = model;
		apiResultObject.setSuccess("Course Valiject Success");
		return apiResultObject.getResult();
	}



}
