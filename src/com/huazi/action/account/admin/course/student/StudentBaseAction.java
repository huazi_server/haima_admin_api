package com.huazi.action.account.admin.course.student;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.action.account.admin.course.CourseBaseAction;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.student.model.StudentCourse;

public class StudentBaseAction<T> extends CourseBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(StudentBaseAction.class);

	public StudentBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public StudentCourse currentStudentCourse;

	public boolean valijectCurrentStudentCourse() {
		log.debug("");

		if(this.valijectCurrentCourse() == false){
			return apiResultObject.getResult();
		}
		if(this.valijectCurrentStudent() == false){
			return apiResultObject.getResult();
		}

		StudentCourse scmodel = new StudentCourse();
		scmodel.setCourseId(this.currentCourse.getId());
		scmodel.setStudentId(this.currentStudent.getId());
		scmodel.setDelFlag(0);
		currentStudentCourse = this.mysqlMgr.getStudentCourseDao().getByExample(scmodel);

		if (currentStudentCourse == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "学生课程不存在");
			return apiResultObject.getResult();
		}
		
		apiResultObject.setSuccess("Student Valiject Success");
		return apiResultObject.getResult();
	}

}
