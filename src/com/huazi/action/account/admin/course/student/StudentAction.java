package com.huazi.action.account.admin.course.student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.util.CommonUtil;

import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CoursePackage;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.teacher.model.Teacher;

public class StudentAction extends StudentBaseAction<StudentCourse>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(StudentAction.class);

	public StudentAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentCourse")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = " where courseId = " + this.currentCourse.getId() + " and delFlag = 0 ";


		where += "order by id desc ";
		
		PageResultObject<StudentCourse> pageimpl = mysqlMgr.getStudentCourseDao().getPageByWhere(pageable, where);
		
		
		docResult.put("pageable",pageimpl.getPageable());
		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		ArrayList<Long> studentids = new ArrayList<Long>();
		ArrayList<Long> teacherids = new ArrayList<Long>();
		for(StudentCourse live : pageimpl.getContent()){
			if(live.getStudentId()!=null)studentids.add(live.getStudentId());
			if(live.getTeacherId()!=null)teacherids.add(live.getTeacherId());
		}
		Map<String,Student> studentmap  = this.mysqlMgr.getStudentDao().getMapByIds(studentids);
		Map<String,Teacher> teachermap  = this.mysqlMgr.getTeacherDao().getMapByIds(teacherids);
		
		
		for(StudentCourse live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getStudentId() != null){
				Student mcs = studentmap.get(live.getStudentId().toString());
				if(mcs != null)livemap.put("studentInfo", mcs.documentBrief());
			}
			if(live.getTeacherId() != null){
				Teacher mcs = teachermap.get(live.getTeacherId().toString());
				if(mcs != null)livemap.put("teacherInfo", mcs.documentBrief());
			}
			pusherlist.add(livemap);
		}
		this.apiResultObject.setSuccess(docResult);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(this.getModel());
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentStudentCourse")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentStudent);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentStudentCourse")})
	public String update(){
		log.debug(""); 

		this.apiResultObject.setSuccess(currentStudentCourse);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentStudentCourse")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentStudent);
		return SUCCESS;
	}
}
