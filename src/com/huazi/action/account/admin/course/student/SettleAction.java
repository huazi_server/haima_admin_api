package com.huazi.action.account.admin.course.student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.teacher.model.TeacherCourse;

public class SettleAction extends StudentBaseAction<StudentCourse>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SettleAction.class);

	public SettleAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("currentStudentCourse"),@Valiject("currentTeacher")})
	public String execute(){
		log.debug(""); 
		
		TeacherCourse tc = new TeacherCourse();
		tc.setTeacherId(currentTeacher.getId());
		tc.setCourseId(currentStudentCourse.getCourseId());
		TeacherCourse temp = this.mysqlMgr.getTeacherCourseDao().getByExample(tc);
		if(temp == null){
			this.apiResultObject.setFailure(ApiResultObject.NotExist, "教师课程不存在");
			return SUCCESS;
		}
		
		this.mysqlMgr.getTeacherService().SettleStudentCourse(currentStudentCourse,temp);
		this.apiResultObject.setSuccess(currentStudentCourse);
		
		return SUCCESS;
	}
	
}
