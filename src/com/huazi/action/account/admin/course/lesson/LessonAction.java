package com.huazi.action.account.admin.course.lesson;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.util.CommonUtil;

import com.huazi.mysql.config.model.CourseLesson;
import com.huazi.mysql.config.model.CoursePackageLesson;

public class LessonAction extends LessonBaseAction<CourseLesson>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LessonAction.class);

	public LessonAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentCourse")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());



		String where = " where courseId = " + this.currentCourse.getId() + " ";
		where += "order by id asc ";
		
		List<CourseLesson> pageimpl = mysqlMgr.getCourseLessonDao().getListByWhere(where);
		
		boolean timenull = false;
		for(CourseLesson lesson : pageimpl){
			if(lesson.getStartAt() == null || lesson.getEndAt() == null){
				timenull = true;
			}
			if(lesson.getStartAt() != null && lesson.getStartAt().getTime() < CommonUtil.GetNowTimestamp().getTime()){
				lesson.setState(1);
				this.mysqlMgr.getCourseLessonDao().update(lesson);
			}
		}
		if(timenull == false && currentCourse.getState() == 0){
			currentCourse.setState(1);
			this.mysqlMgr.getCourseDao().update(currentCourse);
		}

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(this.getModel());
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentLesson")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentLesson")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getTimeMinute() != null){
			currentLesson.setTimeMinute(this.getModel().getTimeMinute());
		}
		if(this.getModel().getKnowledges() != null){
			currentLesson.setKnowledges(this.getModel().getKnowledges());
		}
		if(this.getModel().getShortKnowledges() != null){
			currentLesson.setShortKnowledges(this.getModel().getShortKnowledges());
		}
		if(this.getModel().getStartAt() != null){
			currentLesson.setStartAt(this.getModel().getStartAt());
		}
		if(currentLesson.getStartAt() != null && currentLesson.getTimeMinute() != null){
			currentLesson.setEndAt(CommonUtil.GetTimestampAddMinite(currentLesson.getStartAt(), currentLesson.getTimeMinute()));
		}
		if(this.getModel().getVideoUrl() != null && this.getModel().getVideoUrl().contains("reset")){
			CoursePackageLesson lesson = this.mysqlMgr.getCoursePackageLessonDao().getById(currentLesson.getCoursePackageLessonId());
			if(lesson != null){
				currentLesson.setName(lesson.getName());
				currentLesson.setTitle(lesson.getTitle());
				currentLesson.setVideoUrl(lesson.getVideoUrl());
				currentLesson.setCoverUrl(lesson.getCoverUrl());
				currentLesson.setDocUrl(lesson.getDocUrl());
				currentLesson.setTextbookUrl(lesson.getTextbookUrl());
				currentLesson.setSb3url(lesson.getSb3Url());
				currentLesson.setKnowledges(lesson.getKnowledges());
				currentLesson.setShortKnowledges(lesson.getShortKnowledges());
			}
		}
		mysqlMgr.getCourseLessonDao().update(currentLesson);
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentLesson")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}
}
