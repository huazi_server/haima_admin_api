package com.huazi.action.account.admin.course;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.util.CommonUtil;

import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseGroup;
import com.huazi.mysql.config.model.CoursePackage; 
import com.huazi.mysql.student.model.StudentCourse;

public class CourseAction extends CourseBaseAction<Course>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CourseAction.class);

	public CourseAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());

		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}

		if(this.getAction() != null && this.getAction().contains("count")){
			where += "and state >= 2 ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		} 
		if(this.getModel().getCoursePackageId() != null ){
			where += "and coursePackageId = " + this.getModel().getCoursePackageId() +" ";
		} 
		if(this.getModel().getCourseGroupId() != null ){
			where += "and courseGroupId = " + this.getModel().getCourseGroupId() +" ";
		} 
		if(this.getModel().getPlanTeacherIds() != null ){
			where += "and FIND_IN_SET('" + this.getModel().getPlanTeacherIds() +"',planTeacherIds)>0" + " ";
		}
		
		where += " order by id desc ";
		
		PageResultObject<Course> pageimpl = mysqlMgr.getCourseDao().getPageByWhere(pageable, where);
		List<CoursePackage> list = mysqlMgr.getCoursePackageDao().getListByWhere("");
		Map<String,CoursePackage> packagemap  = mysqlMgr.getCoursePackageDao().getMapByList(list);
		docResult.put("coursePackageInfoMap", packagemap);
		docResult.put("pageable",pageimpl.getPageable());

		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		
		ArrayList<Long> courseids = new ArrayList<Long>();
		for(Course live : pageimpl.getContent()){
			courseids.add(live.getId());
			Document livemap = live.documentBrief();
			if(live.getCoursePackageId() != null){
				CoursePackage mcs = packagemap.get(live.getCoursePackageId().toString());
				if(mcs != null)livemap.put("coursePackageInfo", mcs.documentBrief());
			}
			if(live.getCourseGroupId() != null){
				CourseGroup group = this.mysqlMgr.getCourseGroupDao().getById(live.getCourseGroupId());
				if(group != null)livemap.put("courseGroupInfo", group.documentBrief());
			}
			pusherlist.add(livemap);
		}
		
		
		
		if(this.getAction() != null && this.getAction().contains("count") && courseids.size() > 0){
			String newwhere = " select courseId,count(*) from student_course "
					+ " where courseId in (" + CommonUtil.GetStringList(courseids) + ") and delFlag = 0 "
					+ "and teacherId is NULL group by courseId";
			List<Object>  returns = this.mysqlMgr.getStudentCourseDao().executeResult(newwhere);
			for(Object result : returns){
				if(result != null){
					Object[] results = (Object[]) result;
					long courseId = 0L;
					if(results[0] != null) courseId = Long.parseLong(results[0].toString());
					int count = 0;
					if(results[1] != null) count = Integer.parseInt(results[1].toString());

					for(Document live : pusherlist){
						if(Long.parseLong(live.get("id").toString()) == courseId){
							live.put("settleCount", count);
						}
					}
				}
			}
			newwhere = " select courseId,count(*) from student_course "
					+ " where courseId in (" + CommonUtil.GetStringList(courseids) + ") and delFlag = 0 "
					+ " group by courseId";
			returns = this.mysqlMgr.getStudentCourseDao().executeResult(newwhere);
			for(Object result : returns){
				if(result != null){
					Object[] results = (Object[]) result;
					long courseId = 0L;
					if(results[0] != null) courseId = Long.parseLong(results[0].toString());
					int count = 0;
					if(results[1] != null) count = Integer.parseInt(results[1].toString());

					for(Document live : pusherlist){
						if(Long.parseLong(live.get("id").toString()) == courseId){
							live.put("totalCount", count);
						}
					}
				}
			}
		}
		
		this.apiResultObject.setSuccess(docResult);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin"), @Valiject("currentPackage"), @Valiject("currentGroup-fail")})
	public String create(){
		
		Course course = new Course();
		course.setCoursePackageId(currentPackage.getId());
		course.setStage(currentPackage.getStage());
		course.setCategory(1);
		course.setEnableFlag(1);
		course.setState(0);
		course.setCourseType(1);
		if(currentGroup != null){
			course.setCourseGroupId(currentGroup.getId());
			course.setCourseType(2);
		}
		this.currentCourse = this.mysqlMgr.getCourseDao().insert(course);
		this.mysqlMgr.getConfigService().initCourse(this.currentCourse);
		this.update();
		this.apiResultObject.setSuccess(currentCourse);
		log.debug("");
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentCourse")})
	public String detail(){
		log.debug("");
		this.docResult = currentCourse.document();
		this.docResult.put("coursePackageInfo", mysqlMgr.getCoursePackageDao().getById(currentCourse.getCoursePackageId()));
		this.docResult.put("teacherInfoList",
				mysqlMgr.getTeacherDao().getByIds(CommonUtil.GetLongList(currentCourse.getPlanTeacherIds())));
		this.apiResultObject.setSuccess(this.docResult); 
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentCourse")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getSignEndAt() != null){
			currentCourse.setSignEndAt(this.getModel().getSignEndAt());
		}
		if(this.getModel().getSignStartAt() != null){
			currentCourse.setSignStartAt(this.getModel().getSignStartAt());
		}
		if(this.getModel().getTeachEndAt() != null){
			currentCourse.setTeachEndAt(this.getModel().getTeachEndAt());
		}
		if(this.getModel().getTeachStartAt() != null){
			currentCourse.setTeachStartAt(this.getModel().getTeachStartAt());
		}
		if(this.getModel().getName() != null){
			currentCourse.setName(this.getModel().getName());
		}
		if(this.getModel().getPrice() != null){
			currentCourse.setPrice(this.getModel().getPrice());
		}
		if(this.getModel().getRealPrice() != null){
			currentCourse.setRealPrice(this.getModel().getRealPrice());
		}
		if(this.getModel().getLessonDescription() != null){
			currentCourse.setLessonDescription(this.getModel().getLessonDescription());
		}
		if(this.getModel().getEnrollCount() != null){
			currentCourse.setEnrollCount(this.getModel().getEnrollCount());
		}
		if(this.getModel().getIssue() != null){
			currentCourse.setIssue(this.getModel().getIssue());
		}
		if(this.getModel().getCourseFlag() != null){
			currentCourse.setCourseFlag(this.getModel().getCourseFlag());
		}
		if(this.getModel().getCoverUrl() != null){
			currentCourse.setCoverUrl(this.getModel().getCoverUrl());
		}
		if(this.getModel().getState() != null && this.getModel().getState() == 2 && currentCourse.getState() == 1){
			currentCourse.setState(2);
		}

		if(this.getModel().getPlanTeacherIds() != null && !this.getModel().getPlanTeacherIds().equals(currentCourse.getPlanTeacherIds())){
			currentCourse.setPlanTeacherIds(this.getModel().getPlanTeacherIds());
			this.mysqlMgr.getTeacherService().coursePlanTeachers(currentCourse);
		}
		mysqlMgr.getCourseDao().update(currentCourse);
		this.apiResultObject.setSuccess(currentCourse);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentCourse")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentCourse);
		return SUCCESS;
	}
}
