package com.huazi.action.account.admin.coursepackage;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.config.model.CoursePackage;
import com.huazi.mysql.config.model.CoursePackageLesson;

public class CoursepackageAction extends CoursepackageBaseAction<CoursePackage>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CoursepackageAction.class);

	public CoursepackageAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getModel().getState() != null && this.getModel().getState() != -1){
			where = "where state = " + this.getModel().getState() +" ";
		} 
		
		where += "order by stage desc ";
		
		PageResultObject<CoursePackage> pageimpl = mysqlMgr.getCoursePackageDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");

		if(this.getModel().getLessonCount() < 0 ){
			this.apiResultObject.setFailure(ApiResultObject.NotAllow, "课时数不能小于0");;
			return SUCCESS;
		}
		currentPackage =  mysqlMgr.getCoursePackageDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(currentPackage);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentPackage")})
	public String detail(){
		log.debug("");
		this.docResult = this.mysqlMgr.getConfigService().resetPackageCourse(currentPackage);
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentPackage")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentPackage.setState(this.getModel().getState());
		}		
		if(this.getModel().getName() != null){
			currentPackage.setName(this.getModel().getName());
		}
		if(this.getModel().getCoverUrl() != null){
			currentPackage.setCoverUrl(this.getModel().getCoverUrl());
		}
		if(this.getModel().getRemark() != null){
			currentPackage.setRemark(this.getModel().getRemark());
		}
		if(this.getModel().getLessonCount() != null){
			currentPackage.setLessonCount(this.getModel().getLessonCount());
		}
		if(currentPackage.getLessonCount() == null){
			currentPackage.setLessonCount(0);
		}

		mysqlMgr.getCoursePackageDao().update(currentPackage);
		
		this.docResult = this.mysqlMgr.getConfigService().resetPackageCourse(currentPackage);
		
		
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentPackage")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentPackage);
		return SUCCESS;
	}
}
