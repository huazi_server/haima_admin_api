package com.huazi.action.account.admin.coursepackage.lesson.point;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.CoursePackageLessonVideoPoint;
import com.huazi.mysql.student.model.Student;

public class PointAction extends PointBaseAction<CoursePackageLessonVideoPoint>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PointAction.class);

	public PointAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin"), @Valiject("currentLesson")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "where coursePackagelessonId = " + currentLesson.getId() +" ";
		if(this.getSearchState() == null || this.getSearchState() == -1){
		}
		else{
			where += " and state =  " + this.getSearchState() +" ";
		}

		
		where += "order by id desc ";
		
		PageResultObject<CoursePackageLessonVideoPoint> pageimpl = mysqlMgr.getCoursePackageLessonVideoPointDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin"), @Valiject("currentLesson")})
	public String create(){

		log.debug("");
		this.getModel().setCoursePackageId(currentLesson.getCoursePackageId());
		this.getModel().setCoursePackageLessonId(currentLesson.getId());
		currentPoint =  mysqlMgr.getCoursePackageLessonVideoPointDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(this.getModel());
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentPoint")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentPoint);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentPoint")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getImgUrl() != null){
			currentPoint.setImgUrl(this.getModel().getImgUrl());
		}
		if(this.getModel().getNumber() != null){
			currentPoint.setNumber(this.getModel().getNumber());
		}
		if(this.getModel().getEndTime() != null){
			currentPoint.setEndTime(this.getModel().getEndTime());
		}
		if(this.getModel().getStartTime() != null){
			currentPoint.setStartTime(this.getModel().getStartTime());
		}
		if(this.getModel().getTitle() != null){
			currentPoint.setTitle(this.getModel().getTitle());
		}
		if(this.getModel().getRemark() != null){
			currentPoint.setRemark(this.getModel().getRemark());
		}
		if(this.getModel().getState() != null){
			currentPoint.setState(this.getModel().getState());
		}
		mysqlMgr.getCoursePackageLessonVideoPointDao().update(currentPoint);
		this.apiResultObject.setSuccess(currentPoint);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentPoint")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentPoint);
		return SUCCESS;
	}
}
