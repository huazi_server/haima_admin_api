package com.huazi.action.account.admin.coursepackage.lesson.point;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.coursepackage.lesson.LessonBaseAction;
import com.huazi.mysql.config.model.CoursePackageLessonVideoPoint;

public class PointBaseAction<T> extends LessonBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PointBaseAction.class);

	public PointBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public CoursePackageLessonVideoPoint currentPoint;
	private Long pointId;
	public Long getPointId() {
		return pointId;
	}
	public void setPointId(Long pointId) {
		this.pointId = pointId;
	}
	public boolean valijectCurrentPoint() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getPointId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setPointId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getPointId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "pointId");
			return apiResultObject.getResult();
		}

		long aid = this.getPointId();
		CoursePackageLessonVideoPoint model = mysqlMgr.getCoursePackageLessonVideoPointDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "CoursePackagePoint");
			return apiResultObject.getResult();
		}
		currentPoint = model;
		apiResultObject.setSuccess("CoursePackagePoint Valiject Success");
		return apiResultObject.getResult();
	}



}
