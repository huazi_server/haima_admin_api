package com.huazi.action.account.admin.coursepackage.lesson;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.coursepackage.CoursepackageBaseAction;
import com.huazi.mysql.config.model.CoursePackageLesson;

public class LessonBaseAction<T> extends CoursepackageBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LessonBaseAction.class);

	public LessonBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public CoursePackageLesson currentLesson;
	private Long lessonId;
	public Long getLessonId() {
		return lessonId;
	}
	public void setLessonId(Long lessonId) {
		this.lessonId = lessonId;
	}
	public boolean valijectCurrentLesson() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getLessonId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setLessonId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getLessonId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "lessonId");
			return apiResultObject.getResult();
		}

		long aid = this.getLessonId();
		CoursePackageLesson model = mysqlMgr.getCoursePackageLessonDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "CoursePackageLesson");
			return apiResultObject.getResult();
		}
		currentLesson = model;
		apiResultObject.setSuccess("CoursePackageLesson Valiject Success");
		return apiResultObject.getResult();
	}



}
