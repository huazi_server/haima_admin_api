package com.huazi.action.account.admin.coursepackage.lesson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.CoursePackageLesson;
import com.huazi.mysql.student.model.Student;

public class LessonAction extends LessonBaseAction<CoursePackageLesson>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LessonAction.class);

	public LessonAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}

		where += "order by id desc ";
		
		PageResultObject<Student> pageimpl = mysqlMgr.getStudentDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(this.getModel());
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentLesson")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentLesson")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getCoverUrl() != null){
			currentLesson.setCoverUrl(this.getModel().getCoverUrl());
		}
		if(this.getModel().getImgUrl() != null){
			currentLesson.setImgUrl(this.getModel().getImgUrl());
		}
		if(this.getModel().getName() != null){
			currentLesson.setName(this.getModel().getName());
		}
		if(this.getModel().getNumber() != null){
			currentLesson.setNumber(this.getModel().getNumber());
		}
		if(this.getModel().getTitle() != null){
			currentLesson.setTitle(this.getModel().getTitle());
		}
		if(this.getModel().getKnowledges() != null){
			currentLesson.setKnowledges(this.getModel().getKnowledges());
		}
		if(this.getModel().getShortKnowledges() != null){
			currentLesson.setShortKnowledges(this.getModel().getShortKnowledges());
		}
		if(this.getModel().getVideoUrl() != null){
			currentLesson.setVideoUrl(this.getModel().getVideoUrl());
		}
		if(this.getModel().getDocUrl() != null){
			currentLesson.setDocUrl(this.getModel().getDocUrl());
		}
		if(this.getModel().getTextbookUrl() != null){
			currentLesson.setTextbookUrl(this.getModel().getTextbookUrl());
		}
		if(this.getModel().getSb3Url() != null){
			currentLesson.setSb3Url(this.getModel().getSb3Url());
		}
		if(this.getModel().getRemark() != null){
			currentLesson.setRemark(this.getModel().getRemark());
		}
		currentLesson.setState(1);
		mysqlMgr.getCoursePackageLessonDao().update(currentLesson);
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentLesson")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentLesson);
		return SUCCESS;
	}
}
