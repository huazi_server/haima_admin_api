package com.huazi.action.account.admin.feedback;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.Comment;

public class FeedbackBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FeedbackBaseAction.class);

	public FeedbackBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public Comment currentFeedback;
	private Long teacherId;
	public Long getFeedbackId() {
		return teacherId;
	}
	public void setFeedbackId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public boolean valijectCurrentFeedback() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		
		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getCommentId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setFeedbackId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getFeedbackId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Feedback");
			return apiResultObject.getResult();
		}

		long aid = this.getFeedbackId();
		Comment model = mysqlMgr.getCommentDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Feedback");
			return apiResultObject.getResult();
		}
		currentFeedback = model;
		apiResultObject.setSuccess("Feedback Valiject Success");
		return apiResultObject.getResult();
	}



}
