package com.huazi.action.account.admin.feedback;

import java.util.ArrayList;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.Comment;
import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.student.model.StudentCourseOrder;
import com.huazi.mysql.teacher.model.Teacher;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;

public class FeedbackAction extends FeedbackBaseAction<Comment>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FeedbackAction.class);

	public FeedbackAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());

		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}
		
		if(this.getModel().getType() != null ){
			where += "and type = " + this.getModel().getType() +" ";
		}
		
		where += "order by id desc ";
		
		PageResultObject<Comment> pageimpl = mysqlMgr.getCommentDao().getPageByWhere(pageable, where);


		docResult.put("pageable",pageimpl.getPageable());
		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		ArrayList<Long> studentids = new ArrayList<Long>();
		ArrayList<Long> teacherids = new ArrayList<Long>();
		for(Comment live : pageimpl.getContent()){
			if(live.getType() != null && live.getType() == 2){
				studentids.add(live.getPersonId());
			}
			else{
				teacherids.add(live.getPersonId());
			}
		}
		Map<String,Student> studentmap  = this.mysqlMgr.getStudentDao().getMapByIds(studentids);
		Map<String,Teacher> teachermap  = this.mysqlMgr.getTeacherDao().getMapByIds(teacherids);
		
		
		for(Comment live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getType() != null && live.getType() == 2){
				Student mcs = studentmap.get(live.getPersonId().toString());
				if(mcs != null)livemap.put("userInfo", mcs.documentBrief());
			}
			else{
				Teacher mcs = teachermap.get(live.getPersonId().toString());
				if(mcs != null)livemap.put("userInfo", mcs.documentBrief());
			}
			pusherlist.add(livemap);
		}
		this.apiResultObject.setSuccess(docResult);
		
		return SUCCESS;
	}

	
	@Validations(
		requiredFields={
			@RequiredFieldValidator(fieldName="phone",message="电话号码"),
			@RequiredFieldValidator(fieldName="name",message="姓名"),
		}
	)
	@ValijectList(list = { @Valiject("loginAdmin")})	
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentFeedback")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentFeedback);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentFeedback")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentFeedback.setState(this.getModel().getState());
		}
		mysqlMgr.getCommentDao().update(currentFeedback);
		this.apiResultObject.setSuccess(currentFeedback);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentFeedback")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentFeedback);
		return SUCCESS;
	}
}
