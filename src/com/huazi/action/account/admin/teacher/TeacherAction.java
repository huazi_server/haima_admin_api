package com.huazi.action.account.admin.teacher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.huazi.mysql.teacher.model.Teacher;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;

public class TeacherAction extends TeacherBaseAction<Teacher>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeacherAction.class);

	public TeacherAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());

		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}

		if(this.getModel().getType() != null ){
			where += "and type = " + this.getModel().getType() +" ";
		}
		if(this.getModel().getPhone() != null ){
			where += " and phone like '%" + this.getModel().getPhone() +"%' ";
		}
		if(this.getModel().getName() != null ){
			where += " and name like '%" + this.getModel().getName() +"%' ";
		}
		
		where += "order by id desc ";
		
		PageResultObject<Teacher> pageimpl = mysqlMgr.getTeacherDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@Validations(
		requiredFields={
			@RequiredFieldValidator(fieldName="phone",message="电话号码"),
			@RequiredFieldValidator(fieldName="name",message="姓名"),
		}
	)
	@ValijectList(list = { @Valiject("loginAdmin")})	
	public String create(){
		log.debug("");
		Teacher model = new Teacher();
		model.setPhone(this.getModel().getPhone());
		Teacher teacher = mysqlMgr.getTeacherDao().getByExample(model);
		if(teacher != null){
			this.apiResultObject.setFailure(ApiResultObject.HasExist, "电话号码已经存在");
			return SUCCESS;
		}
		else{
			model.setName(this.getModel().getName());
			model.setNickName(this.getModel().getNickName());
			model.setRegisterAt(CommonUtil.GetNowTimestamp());
			model.setState(2);
			model.setType(2);
			mysqlMgr.getTeacherDao().insert(model);
			
			currentTeacher = model;
			this.update();
			this.apiResultObject.setSuccess(model);
			return SUCCESS;
		}
	}
	
	@ValijectList(list = { @Valiject("currentTeacher")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentTeacher);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentTeacher")})
	public String update(){
		log.debug(""); 
		Teacher model = currentTeacher ;
		if(this.getModel().getAvatarUrl() != null){
			model.setAvatarUrl(this.getModel().getAvatarUrl());
		}
		if(this.getModel().getInfo() != null){
			model.setInfo(this.getModel().getInfo());
		}
		if(this.getModel().getGender() != null){
			model.setGender(this.getModel().getGender());
		}
		if(this.getModel().getState() != null){
			model.setState(this.getModel().getState());
		}
		if(this.getModel().getName() != null){
			model.setName(this.getModel().getName());
		}
		if(this.getModel().getNickName() != null){ 
			model.setNickName(this.getModel().getNickName());
		}
		if(this.getModel().getPhone() != null){
			model.setPhone(this.getModel().getPhone());
		}
		if(this.getModel().getQrCodeUrl() != null){
			model.setQrCodeUrl(this.getModel().getQrCodeUrl());
		}
		
		//QcloudImUtil.CreateAccount(model.getChatAccount(),model.getNickname(),model.getAvatarUrl());
		
		mysqlMgr.getTeacherDao().update(model);
		this.apiResultObject.setSuccess(model);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentTeacher")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentTeacher);
		return SUCCESS;
	}
}
