package com.huazi.action.account.admin.teacher;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.teacher.model.Teacher;

public class TeacherBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(TeacherBaseAction.class);

	public TeacherBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public Teacher currentTeacher;
	private Long teacherId;
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public boolean valijectCurrentTeacher() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		
		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getTeacherId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setTeacherId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getTeacherId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Teacher");
			return apiResultObject.getResult();
		}

		long aid = this.getTeacherId();
		Teacher model = mysqlMgr.getTeacherDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Teacher");
			return apiResultObject.getResult();
		}
		currentTeacher = model;
		apiResultObject.setSuccess("Teacher Valiject Success");
		return apiResultObject.getResult();
	}



}
