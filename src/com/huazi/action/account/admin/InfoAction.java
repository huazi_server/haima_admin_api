package com.huazi.action.account.admin;

import java.sql.Timestamp;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.student.model.Student;

public class InfoAction extends AdminBaseAction<Student>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(InfoAction.class);

	public InfoAction(){
		//this.mysqlMgr.getAccountDao().getCountByWhere(null, "fsadf asdf", "");
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug("");


		Document dom = loginAdmin.document();
		this.apiResultObject.setSuccess(dom);
		return SUCCESS;
		
	}

}
