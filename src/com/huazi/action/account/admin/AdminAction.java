package com.huazi.action.account.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.huazi.mysql.admin.model.Admin;

public class AdminAction extends AdminBaseAction<Admin>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AdminAction.class);

	public AdminAction(){
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		
		JSONObject apiObject=null;
		try {
			apiObject = JSON.parseObject("{}");
			JSONObject formEdit = JSON.parseObject("{}");
			JSONArray columns = JSON.parseArray("[]");
			columns.add(JSON.parseObject("{type:'string',value:'',name:'name',title:'Name',pattern:''}"));
			columns.add(JSON.parseObject("{type:'string',value:'',name:'title',title:'Title',pattern:''}"));
			
			

			JSONObject searchDefine = JSON.parseObject("{}");
			searchDefine.put("typeAllow", JSON.parseObject("[[0,'Name'],[1,'Title'],[2,'Description']]"));
			searchDefine.put("stateAllow", JSON.parseObject("[[0,'禁用','btn-danger'],[1,'启用','btn-success']]"));
			searchDefine.put("orderAllow", JSON.parseObject("[[0,'更新'],[1,'创建']]"));
			
			formEdit.put("columns", columns);
			apiObject.put("formEdit",formEdit);
			apiObject.put("searchDefine",searchDefine);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		this.apiResultObject.setSuccess(apiObject);
		
		/*
		$scope.searchTypeAllow=[[0,'Name'],[1,'Title'],[2,'Description']];
		$scope.searchStateAllow=[[0,'禁用','btn-danger'],[1,'启用','btn-success']];
		$scope.searchOrderAllow=[[0,'更新'],[1,'创建']];
						
		formEdit={name:"",columns:[]};
		formEdit.name="增加model";
		formEdit.result="";
		formEdit.columns[0] = {type:"string",value:"",name:"name",title:"Name",pattern:/^([a-zA-Z][a-zA-Z0-9]{2,20})$/i};
		formEdit.columns[1] = {type:"string",value:"",name:"title",title:"Title",pattern:/^([\u4e00-\u9fa5a-zA-Z][\u4e00-\u9fa5a-zA-Z0-9]{1,20})$/i};
		formEdit.columns[2] = {type:"string",value:"",name:"description",title:"Description",pattern:/^([\u4e00-\u9fa5a-zA-Z][\u4e00-\u9fa5a-zA-Z0-9]{1,20})$/i};
		formEdit.columns[3] = {type:"date",value:"",name:"createdAt",title:"CreatedAt",format:$rootScope.datetimeMomentFormat};
		 */
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String update(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String manage(){
		log.debug("");
		if(this.getAction() == null){
			this.apiResultObject.setSuccess(loginAdmin);
			return SUCCESS;
		}
		if(this.getAction().compareTo("queryPay") == 0){
		}
		if(this.getAction().compareTo("queryRefund") == 0){
		}
		this.apiResultObject.setSuccess(loginAdmin);
		return SUCCESS;
	}
}
