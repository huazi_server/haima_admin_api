package com.huazi.action.account.admin.report;

import java.util.ArrayList;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.Production;
import com.huazi.mysql.config.model.ProductionComment;
import com.huazi.mysql.config.model.ProductionCommentReply;
import com.huazi.mysql.config.model.Report;
import com.huazi.mysql.config.model.ReportConfig;
import com.huazi.mysql.student.model.Student;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;

public class ReportAction extends ReportBaseAction<Report>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ReportAction.class);

	public ReportAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());

		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}
		
		if(this.getModel().getType() != null ){
			where += "and type = " + this.getModel().getType() +" ";
		}

		
		where += "order by id desc ";
		
		PageResultObject<Report> pageimpl = mysqlMgr.getReportDao().getPageByWhere(pageable, where);

		docResult.put("pageable",pageimpl.getPageable());
		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		ArrayList<Long> studentids = new ArrayList<Long>();
		ArrayList<Long> teacherids = new ArrayList<Long>();
		ArrayList<Long> productids = new ArrayList<Long>();
		ArrayList<Long> productcommentids = new ArrayList<Long>();
		ArrayList<Long> productreplyids = new ArrayList<Long>();
		for(Report live : pageimpl.getContent()){
			studentids.add(live.getPlaintiffId());
			teacherids.add(live.getReportConfigId());
			productids.add(live.getDefendantId());
			if(live.getDefendantId() != null){
				if(live.getType() != null && live.getType() == 1){
					productids.add(live.getDefendantId());
				}
				else if(live.getType() != null && live.getType() == 2){
					productcommentids.add(live.getDefendantId());
				}
				else if(live.getType() != null && live.getType() == 3){
					productreplyids.add(live.getDefendantId());
				}
			}
		}
		Map<String,Student> studentmap  = this.mysqlMgr.getStudentDao().getMapByIds(studentids);
		Map<String,ReportConfig> teachermap  = this.mysqlMgr.getReportConfigDao().getMapByIds(teacherids);
		Map<String,Production> pmap  = this.mysqlMgr.getProductionDao().getMapByIds(productids);
		Map<String,ProductionComment> pmmap  = this.mysqlMgr.getProductionCommentDao().getMapByIds(productcommentids);
		Map<String,ProductionCommentReply> pmrmap  = this.mysqlMgr.getProductionCommentReplyDao().getMapByIds(productreplyids);
		
		
		for(Report live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getPlaintiffId() != null){
				Student mcs = studentmap.get(live.getPlaintiffId().toString());
				if(mcs != null)livemap.put("studentInfo", mcs.documentBrief());
			}
			if(live.getReportConfigId() != null){
				ReportConfig mcs = teachermap.get(live.getReportConfigId().toString());
				if(mcs != null)livemap.put("configInfo", mcs.documentBrief());
			}
			if(live.getDefendantId() != null){
				if(live.getType() != null && live.getType() == 1){
					Production mcs = pmap.get(live.getDefendantId().toString());
					if(mcs != null)livemap.put("reportInfo", mcs.documentBrief());
				}
				else if(live.getType() != null && live.getType() == 2){
					ProductionComment mcs = pmmap.get(live.getDefendantId().toString());
					if(mcs != null)livemap.put("reportInfo", mcs.documentBrief());
				}
				else if(live.getType() != null && live.getType() == 3){
					ProductionCommentReply mcs = pmrmap.get(live.getDefendantId().toString());
					if(mcs != null)livemap.put("reportInfo", mcs.documentBrief());
				}
			}
			pusherlist.add(livemap);
		}
		this.apiResultObject.setSuccess(docResult);
		return SUCCESS;
	}

	
	@Validations(
		requiredFields={
			@RequiredFieldValidator(fieldName="phone",message="电话号码"),
			@RequiredFieldValidator(fieldName="name",message="姓名"),
		}
	)
	@ValijectList(list = { @Valiject("loginAdmin")})	
	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(currentReport);
		return SUCCESS;

	}
	
	@ValijectList(list = { @Valiject("currentReport")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentReport);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentReport")})
	public String update(){
		log.debug(""); 
		Report model = currentReport ;
		if(this.getModel().getState() != null){
			model.setState(this.getModel().getState());
			
			if(this.getModel().getState() == 2 && currentReport.getDefendantId() != null && currentReport.getType() != null){
				if(currentReport.getType() == 1){
					Production mcs = this.mysqlMgr.getProductionDao().getById(currentReport.getDefendantId());
					if(mcs!= null){
						mcs.setState(3);
						this.mysqlMgr.getProductionDao().update(mcs);
					}
				}
				else if(currentReport.getType() == 2){
					ProductionComment mcs = this.mysqlMgr.getProductionCommentDao().getById(currentReport.getDefendantId());
					if(mcs!= null){
						mcs.setDelFlag(1);;
						this.mysqlMgr.getProductionCommentDao().update(mcs);
					}
				}
				else if(currentReport.getType() == 3){
					ProductionCommentReply mcs = this.mysqlMgr.getProductionCommentReplyDao().getById(currentReport.getDefendantId());
					if(mcs!= null){
						mcs.setDelFlag(1);;
						this.mysqlMgr.getProductionCommentReplyDao().update(mcs);
					}
				}
			}
		}
		
		mysqlMgr.getReportDao().update(model);
		this.apiResultObject.setSuccess(model);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentReport")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentReport);
		return SUCCESS;
	}
}
