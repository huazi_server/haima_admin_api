package com.huazi.action.account.admin.report;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.Report;

public class ReportBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ReportBaseAction.class);

	public ReportBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public Report currentReport;
	private Long teacherId;
	public Long getReportId() {
		return teacherId;
	}
	public void setReportId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public boolean valijectCurrentReport() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		
		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getReportId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setReportId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getReportId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Report");
			return apiResultObject.getResult();
		}

		long aid = this.getReportId();
		Report model = mysqlMgr.getReportDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Report");
			return apiResultObject.getResult();
		}
		currentReport = model;
		apiResultObject.setSuccess("Report Valiject Success");
		return apiResultObject.getResult();
	}



}
