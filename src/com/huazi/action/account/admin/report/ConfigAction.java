package com.huazi.action.account.admin.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.ReportConfig;
import com.huazi.mysql.student.model.StudentCourse;

public class ConfigAction extends ReportBaseAction<StudentCourse>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ConfigAction.class);

	public ConfigAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug(""); 


		this.apiResultObject.setSuccess(this.mysqlMgr.getReportConfigDao().getByExample(new ReportConfig()));
		
		return SUCCESS;
	}
	
}
