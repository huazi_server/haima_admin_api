package com.huazi.action.account.admin;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.mysql.admin.model.Admin;
import com.huazi.mysql.config.model.CourseGroup;
import com.huazi.mysql.config.model.CoursePackage;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.teacher.model.Teacher;

public class AdminBaseAction<T> extends AccountBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AdminBaseAction.class);

	public AdminBaseAction() {
		log.debug("");
	}

	public Admin loginAdmin;

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}


	public boolean valijectLoginAdmin() {
		log.debug("");

		if(this.valijectLoginAccount() == false){
			return apiResultObject.getResult();
		}

		loginAdmin = null;
		
		apiResultObject.setFailure(ApiResultObject.NotLogin, "管理员未登录");
		if( this.loginAccount.getString("type").equals("admin")){
			Admin model = new Admin();
			model.setAccountId(this.loginAccount.getLong("id"));
			log.debug(this.loginAccount.getLong("id")+"");
			loginAdmin = mysqlMgr.getAdminDao().getByExample(model); 
			apiResultObject.setFailureData(ApiResultObject.NotLogin,loginAdmin, "管理员账户不存在"); 
			
			if(loginAdmin != null){ 
				apiResultObject.setSuccess("Admin Valiject Success");
			}
		}

		return apiResultObject.getResult();
	}


	public Student currentStudent;
	private Long studentId;
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public boolean valijectCurrentStudent() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getStudentId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setStudentId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getStudentId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Student");
			return apiResultObject.getResult();
		}

		long aid = this.getStudentId();
		Student model = mysqlMgr.getStudentDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Student");
			return apiResultObject.getResult();
		}
		currentStudent = model;
		apiResultObject.setSuccess("Student Valiject Success");
		return apiResultObject.getResult();
	}


	public Teacher currentTeacher;
	private Long teacherId;
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public boolean valijectCurrentTeacher() {
		log.debug("");
		
		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}
		
		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getTeacherId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setTeacherId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getTeacherId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Teacher");
			return apiResultObject.getResult();
		}

		long aid = this.getTeacherId();
		Teacher model = mysqlMgr.getTeacherDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Teacher");
			return apiResultObject.getResult();
		}
		currentTeacher = model;
		apiResultObject.setSuccess("Teacher Valiject Success");
		return apiResultObject.getResult();
	}

	public CoursePackage currentPackage;
	private Long packageId;
	public Long getPackageId() {
		return packageId;
	}
	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}
	public boolean valijectCurrentPackage() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getPackageId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setPackageId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getPackageId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Package");
			return apiResultObject.getResult();
		}

		long aid = this.getPackageId();
		CoursePackage model = mysqlMgr.getCoursePackageDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Package");
			return apiResultObject.getResult();
		}
		currentPackage = model;
		apiResultObject.setSuccess("Package Valiject Success");
		return apiResultObject.getResult();
	}


	public CourseGroup currentGroup;
	private Long groupId;
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public boolean valijectCurrentGroup() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getGroupId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setGroupId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getGroupId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Group");
			return apiResultObject.getResult();
		}

		long aid = this.getGroupId();
		CourseGroup model = mysqlMgr.getCourseGroupDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Group");
			return apiResultObject.getResult();
		}
		currentGroup = model;
		apiResultObject.setSuccess("Group Valiject Success");
		return apiResultObject.getResult();
	}

}
