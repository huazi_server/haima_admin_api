package com.huazi.action.account.admin.activity.free;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.config.model.ActivityFree;
import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseGroup;

public class FreeAction extends FreeBaseAction<ActivityFree>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FreeAction.class);

	public FreeAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "where 1 = 1 ";
		if(this.getSearchState() != null && this.getSearchState() != -1){
				where += " and state = " + this.getSearchState() +" ";
		} 

		if(this.getModel().getState() != null && this.getModel().getState() != -1){
			if(this.getModel().getState() != null){
				where += " and state <= " + this.getModel().getState() +" ";
			}
			else{
				where += " and state = " + this.getModel().getState() +" ";
			}
		}
		
		where += " order by id desc ";
		
		PageResultObject<ActivityFree> pageimpl = mysqlMgr.getActivityFreeDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		
		if(this.getModel().getCourseType() == null){
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "请传课程类型");
			return SUCCESS;
		}

		if(this.getModel().getCourseType() == 1){
			Course course = this.mysqlMgr.getCourseDao().getById(this.getModel().getCourseId());
			if(course == null){
				apiResultObject.setFailure(ApiResultObject.NotExist, "课程不存在");
				return SUCCESS;
			}
			this.getModel().setCourseGroupId(null);
		}
		if(this.getModel().getCourseType() == 2){
			CourseGroup course = this.mysqlMgr.getCourseGroupDao().getById(this.getModel().getCourseGroupId());
			if(course == null){
				apiResultObject.setFailure(ApiResultObject.NotExist, "年课不存在");
				return SUCCESS;
			}
			this.getModel().setCourseId(null);
		}
		
		currentActivityFree =  mysqlMgr.getActivityFreeDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(currentActivityFree);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentActivityFree")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(this.currentActivityFree);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentActivityFree")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentActivityFree.setState(this.getModel().getState());
		}		
		if(this.getModel().getName() != null){
			currentActivityFree.setName(this.getModel().getName());
		}	
		if(this.getModel().getDest() != null){
			currentActivityFree.setDest(this.getModel().getDest());
		}
		if(this.getModel().getIntro() != null){
			currentActivityFree.setIntro(this.getModel().getIntro());
		}
		if(this.getModel().getQuotaCount() != null){
			currentActivityFree.setQuotaCount(this.getModel().getQuotaCount());
		}
		if(this.getModel().getImgUrl() != null){
			currentActivityFree.setImgUrl(this.getModel().getImgUrl());
		}
		if(this.getModel().getCourseType() != null){
			currentActivityFree.setCourseType(this.getModel().getCourseType());
		}
		if(this.getModel().getRemark() != null){
			currentActivityFree.setRemark(this.getModel().getRemark());
		}
		mysqlMgr.getActivityFreeDao().update(currentActivityFree);
		
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentActivityFree")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentActivityFree);
		return SUCCESS;
	}
}
