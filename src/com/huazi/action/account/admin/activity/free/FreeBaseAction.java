package com.huazi.action.account.admin.activity.free;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.ActivityFree;

public class FreeBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FreeBaseAction.class);

	public FreeBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}



	public ActivityFree currentActivityFree;
	private Long channelId;
	public Long getActivityFreeId() {
		return channelId;
	}
	public void setActivityFreeId(Long channelId) {
		this.channelId = channelId;
	}
	public boolean valijectCurrentActivityFree() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getActivityFreeId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setActivityFreeId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getActivityFreeId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "ActivityFree");
			return apiResultObject.getResult();
		}

		long aid = this.getActivityFreeId();
		ActivityFree model = mysqlMgr.getActivityFreeDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "ActivityFree");
			return apiResultObject.getResult();
		}
		currentActivityFree = model;
		apiResultObject.setSuccess("ActivityFree Valiject Success");
		return apiResultObject.getResult();
	}

}
