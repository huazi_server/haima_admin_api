package com.huazi.action.account.admin.channel.activity;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.ChannelActivity;

public class ActivityBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ActivityBaseAction.class);

	public ActivityBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}



	public ChannelActivity currentChannelActivity;
	private Long channelActivityId;
	public Long getChannelActivityId() {
		return channelActivityId;
	}
	public void setChannelActivityId(Long channelActivityId) {
		this.channelActivityId = channelActivityId;
	}
	public boolean valijectCurrentChannelActivity() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getChannelActivityId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setChannelActivityId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getChannelActivityId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "ChannelActivity");
			return apiResultObject.getResult();
		}

		long aid = this.getChannelActivityId();
		ChannelActivity model = mysqlMgr.getChannelActivityDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "ChannelActivity");
			return apiResultObject.getResult();
		}
		currentChannelActivity = model;
		apiResultObject.setSuccess("ChannelActivity Valiject Success");
		return apiResultObject.getResult();
	}

}
