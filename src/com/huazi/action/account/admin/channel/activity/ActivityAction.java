package com.huazi.action.account.admin.channel.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.config.model.ActivityFree;
import com.huazi.mysql.config.model.Channel;
import com.huazi.mysql.config.model.ChannelActivity;
import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseGroup;
import com.huazi.mysql.config.model.CoursePackage;

public class ActivityAction extends ActivityBaseAction<ChannelActivity>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ActivityAction.class);

	public ActivityAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "where 1 = 1 ";
		if(this.getSearchState() != null && this.getSearchState() != -1){
				where += " and state = " + this.getSearchState() +" ";
		} 

		if(this.getModel().getState() != null && this.getModel().getState() != -1){
			if(this.getModel().getState() != null){
				where += " and state <= " + this.getModel().getState() +" ";
			}
			else{
				where += " and state = " + this.getModel().getState() +" ";
			}
		}

		if(this.getModel().getChannelId() != null && this.getModel().getChannelId() != -1){
			where += " and channelId = " + this.getModel().getChannelId() +" ";
		}
		where += " order by id desc ";
		
		PageResultObject<ChannelActivity> pageimpl = mysqlMgr.getChannelActivityDao().getPageByWhere(pageable, where);

		docResult.put("pageable",pageimpl.getPageable());

		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		
		for(ChannelActivity live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getChannelId() != null){
				Channel mcs = this.mysqlMgr.getChannelDao().getById(live.getChannelId());
				if(mcs != null)livemap.put("channelInfo", mcs.documentBrief());
			}
			if(live.getActivityType() != null){
				if(live.getActivityType() == 1){
					ActivityFree group = this.mysqlMgr.getActivityFreeDao().getById(live.getActivityId());
					if(group != null)livemap.put("activityFreeInfo", group.documentBrief());
				}
			}
			pusherlist.add(livemap);
		}
		
		
		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		
		currentChannelActivity =  mysqlMgr.getChannelActivityDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(currentChannelActivity);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentChannelActivity")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(this.currentChannelActivity);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentChannelActivity")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentChannelActivity.setState(this.getModel().getState());
		}		
		
		if(this.getModel().getRemark() != null){
			currentChannelActivity.setRemark(this.getModel().getRemark());
		}
		mysqlMgr.getChannelActivityDao().update(currentChannelActivity);
		
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentChannelActivity")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentChannelActivity);
		return SUCCESS;
	}
}
