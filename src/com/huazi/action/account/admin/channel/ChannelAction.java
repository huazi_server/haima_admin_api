package com.huazi.action.account.admin.channel;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.mysql.config.model.Channel;

public class ChannelAction extends ChannelBaseAction<Channel>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ChannelAction.class);

	public ChannelAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		/*
			测试地址： https://www.haimacode.com/test/HaimaStudent/gzh/index.html
			正式地址：https://www.haimacode.com/s/gzh/index.html
			活动测试地址：https://www.haimacode.com/test/HaimaStudent/gzh/index.html#/promotion/:hdid
			作业评论测试地址:https://www.haimacode.com/test/HaimaStudent/gzh/index.html#/homework-comments/:hwid
			年课购买测试地址：https://www.haimacode.com/test/HaimaStudent/gzh/index.html#/course-series
			体验课购买测试地址：https://www.haimacode.com/test/HaimaStudent/gzh/index.html#/experience-lesson
			说明：路由中包含：部分替换相应id，如需加入分发渠道统计，可在链接后增加’?channelId=***‘,例如：
			https://www.haimacode.com/test/HaimaStudent/gzh/index.html#/course-series?channelId=10001,目前定的channelId为10001，
			channelId后台指定！正式环境请替换正式地址。
		*/
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "where 1 = 1 ";
		if(this.getSearchState() != null && this.getSearchState() != -1){
				where += " and state = " + this.getSearchState() +" ";
		} 

		if(this.getModel().getState() != null && this.getModel().getState() != -1){
			if(this.getModel().getState() != null){
				where += " and state <= " + this.getModel().getState() +" ";
			}
			else{
				where += " and state = " + this.getModel().getState() +" ";
			}
		} 
		
		where += " order by id desc ";
		
		PageResultObject<Channel> pageimpl = mysqlMgr.getChannelDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		currentChannel =  mysqlMgr.getChannelDao().insert(this.getModel());
		this.update();
		this.apiResultObject.setSuccess(currentChannel);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentChannel")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(this.currentChannel);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentChannel")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getState() != null){
			currentChannel.setState(this.getModel().getState());
		}		
		if(this.getModel().getName() != null){
			currentChannel.setName(this.getModel().getName());
		}	
		if(this.getModel().getInfo() != null){
			currentChannel.setInfo(this.getModel().getInfo());
		}
		if(this.getModel().getAddress() != null){
			currentChannel.setAddress(this.getModel().getAddress());
		}
		if(this.getModel().getNumber() != null){
			currentChannel.setNumber(this.getModel().getNumber());
		}
		if(this.getModel().getRemark() != null){
			currentChannel.setRemark(this.getModel().getRemark());
		}
		mysqlMgr.getChannelDao().update(currentChannel);
		
		this.apiResultObject.setSuccess(this.docResult);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentChannel")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentChannel);
		return SUCCESS;
	}
}
