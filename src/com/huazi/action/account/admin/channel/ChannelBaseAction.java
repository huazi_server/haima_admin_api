package com.huazi.action.account.admin.channel;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.mysql.config.model.Channel;

public class ChannelBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ChannelBaseAction.class);

	public ChannelBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}



	public Channel currentChannel;
	private Long channelId;
	public Long getChannelId() {
		return channelId;
	}
	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}
	public boolean valijectCurrentChannel() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getChannelId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setChannelId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getChannelId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Channel");
			return apiResultObject.getResult();
		}

		long aid = this.getChannelId();
		Channel model = mysqlMgr.getChannelDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Channel");
			return apiResultObject.getResult();
		}
		currentChannel = model;
		apiResultObject.setSuccess("Channel Valiject Success");
		return apiResultObject.getResult();
	}

}
