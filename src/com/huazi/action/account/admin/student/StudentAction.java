package com.huazi.action.account.admin.student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.teacher.model.Teacher;

public class StudentAction extends StudentBaseAction<Student>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(StudentAction.class);

	public StudentAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = " where 1 = 1 ";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}
		
		if(this.getSearchString() != null && CommonUtil.GetStringLong(this.getSearchString()) != null){
			where += " and studentId = " + this.getSearchString() +" ";
		}

		if(this.getModel().getPhone() != null ){
			where += " and phone like '%" + this.getModel().getPhone() +"%' ";
		}
		if(this.getModel().getName() != null ){
			where += " and name like '%" + this.getModel().getName() +"%' ";
		}
		
		where += "order by id desc ";
		
		PageResultObject<Student> pageimpl = mysqlMgr.getStudentDao().getPageByWhere(pageable, where);

		this.apiResultObject.setSuccess(pageimpl);
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		Student model = new Student();
		model.setPhone(this.getModel().getPhone());
		Student teacher = mysqlMgr.getStudentDao().getByExample(model);
		if(teacher != null){
			this.apiResultObject.setFailure(ApiResultObject.HasExist, "电话号码已经存在");
			return SUCCESS;
		}
		else{
			model.setName(this.getModel().getName());
			model.setNickName(this.getModel().getNickName());
			model.setRegisterAt(CommonUtil.GetNowTimestamp());
			mysqlMgr.getStudentDao().insert(model);
			this.apiResultObject.setSuccess(model);
			return SUCCESS;
		}
	}
	
	@ValijectList(list = { @Valiject("currentStudent")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentStudent);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentStudent")})
	public String update(){
		log.debug(""); 
		if(this.getModel().getAvatarUrl() != null){
			currentStudent.setAvatarUrl(this.getModel().getAvatarUrl());
		}
		if(this.getModel().getGender() != null){
			currentStudent.setGender(this.getModel().getGender());
		}
		if(this.getModel().getInfo() != null){
			currentStudent.setInfo(this.getModel().getInfo());
		}
		if(this.getModel().getState() != null){
			currentStudent.setState(this.getModel().getState());
		}
		
		//QcloudImUtil.CreateAccount(model.getChatAccount(),model.getNickname(),model.getAvatarUrl());
		
		mysqlMgr.getStudentDao().update(currentStudent);
		this.apiResultObject.setSuccess(currentStudent);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentStudent")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentStudent);
		return SUCCESS;
	}
}
