package com.huazi.action.account.admin.student.order;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.AccountBaseAction;
import com.huazi.action.account.admin.AdminBaseAction;
import com.huazi.action.account.admin.student.StudentBaseAction;
import com.huazi.mysql.student.model.StudentCourseOrder;

public class OrderBaseAction<T> extends StudentBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(OrderBaseAction.class);

	public OrderBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	public StudentCourseOrder currentOrder;
	private Long orderId;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public boolean valijectCurrentOrder() {
		log.debug("");

		if(this.valijectLoginAdmin() == false){
			return apiResultObject.getResult();
		}

		try {
			Method valijectMethod;
			valijectMethod = this.getModel().getClass().getMethod("getOrderId", new Class[0]);
			if(valijectMethod!=null){
				long aId = (long) valijectMethod.invoke(this.getModel(), new Object[0]);
				this.setOrderId(aId);
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} 

		if (this.getOrderId()== null) {
			apiResultObject.setFailure(ApiResultObject.ValijectInput, "Order");
			return apiResultObject.getResult();
		}

		long aid = this.getOrderId();
		StudentCourseOrder model = mysqlMgr.getStudentCourseOrderDao().getById(aid);

		if (model == null ) {
			apiResultObject.setFailure(ApiResultObject.NotExist, "Order");
			return apiResultObject.getResult();
		}
		currentOrder = model;
		apiResultObject.setSuccess("Order Valiject Success");
		return apiResultObject.getResult();
	}



}
