package com.huazi.action.account.admin.student.order;

import java.util.ArrayList;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.action.PageResultObject;
import sshbase.struts2.action.PageableObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.util.CommonUtil;

import com.huazi.mysql.config.model.Course;
import com.huazi.mysql.config.model.CourseGroup;
import com.huazi.mysql.student.model.Student;
import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.student.model.StudentCourseOrder;
import com.huazi.mysql.teacher.model.Teacher;


public class OrderAction extends OrderBaseAction<StudentCourseOrder>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(OrderAction.class);

	public OrderAction(){
		log.debug("");
	}

	@Override
	public String execute(){
		log.debug("");
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("loginAdmin")})
	public String search(){
		log.debug("");
		PageableObject pageable = this.getPageable();
		log.debug("pageable " + pageable.getPageSize());


		String where = "";
		if(this.getSearchState() == null || this.getSearchState() == -1){
			where = "where 1 = 1";
		}
		else{
			where = "where state = " + this.getSearchState() +" ";
		}
		if(this.getModel().getState() != null ){
			where += "and state = " + this.getModel().getState() +" ";
		}

		if(this.getModel().getPhone() != null ){
			where += " and phone like '%" + this.getModel().getPhone() +"%' ";
		}

		where += "order by id desc ";
		
		PageResultObject<StudentCourseOrder> pageimpl = mysqlMgr.getStudentCourseOrderDao().getPageByWhere(pageable, where);


		docResult.put("pageable",pageimpl.getPageable());
		ArrayList<Document> pusherlist = new ArrayList<Document>();
		docResult.put("content",pusherlist);
		ArrayList<Long> studentids = new ArrayList<Long>();
		ArrayList<Long> teacherids = new ArrayList<Long>();
		ArrayList<Long> groupids = new ArrayList<Long>();
		for(StudentCourseOrder live : pageimpl.getContent()){
			studentids.add(live.getStudentId());
			if(live.getCourseType() == 1)teacherids.add(CommonUtil.GetStringLong(live.getCourseIds()));
			if(live.getCourseType() == 2)groupids.add(live.getCourseGroupId());
		}
		Map<String,Student> studentmap  = this.mysqlMgr.getStudentDao().getMapByIds(studentids);
		Map<String,Course> teachermap  = this.mysqlMgr.getCourseDao().getMapByIds(teacherids);
		Map<String,CourseGroup> groupmap  = this.mysqlMgr.getCourseGroupDao().getMapByIds(groupids);
		
		
		for(StudentCourseOrder live : pageimpl.getContent()){
			Document livemap = live.documentBrief();
			if(live.getStudentId() != null){
				Student mcs = studentmap.get(live.getStudentId().toString());
				if(mcs != null)livemap.put("studentInfo", mcs.documentBrief());
			}
			if(live.getCourseType() == 1){
				Course mcs = teachermap.get(live.getCourseIds().toString());
				if(mcs != null)livemap.put("courseInfo", mcs.documentBrief());
			}
			if(live.getCourseType() == 2){
				CourseGroup mcs = groupmap.get(live.getCourseGroupId().toString());
				if(mcs != null)livemap.put("courseGroupInfo", mcs.documentBrief());
			}
			pusherlist.add(livemap);
		}
		this.apiResultObject.setSuccess(docResult);
		
		return SUCCESS;
	}

	
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String create(){
		log.debug("");
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentOrder")})
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(currentOrder);
		return SUCCESS;
	}

	@ValijectList(list = { @Valiject("currentOrder")})
	public String update(){
		log.debug(""); 
		StudentCourseOrder model = mysqlMgr.getStudentCourseOrderDao().getById(currentOrder.getId());

		if(this.getModel().getState() != null){
			model.setState(this.getModel().getState());
		}
		
		mysqlMgr.getStudentCourseOrderDao().update(model);
		this.apiResultObject.setSuccess(model);
		return SUCCESS;
	}
	
	@ValijectList(list = { @Valiject("currentOrder")})
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(currentOrder);
		return SUCCESS;
	}
}
