package com.huazi.action.account.admin.student.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import s3part.service.AccountService;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.mysql.student.model.StudentCourse;
import com.huazi.mysql.student.model.StudentCourseOrder;
import com.huazi.mysql.teacher.model.TeacherCourse;

public class RefundAction extends OrderBaseAction<StudentCourseOrder>{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RefundAction.class);

	public RefundAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("currentStudent"),@Valiject("currentOrder")})
	public String execute(){
		log.debug(""); 

		if(currentOrder.getState() != 3 && currentOrder.getState() != 4){
			this.apiResultObject.setFailure(ApiResultObject.NotExist, "订单状态不正确,不能退款");
			return SUCCESS;
		}
		if(this.getModel().getRefundMoney() == null){
			this.apiResultObject.setFailure(ApiResultObject.NotExist, "请输入退款金额");
			return SUCCESS;
		};
		
		if(currentOrder.getStudentId().longValue() == currentStudent.getId().longValue()){
			
			ApiResultObject obj = AccountService.refundOrder(this.getLoginToken(), currentOrder.getOrderNo(), (int)(this.getModel().getRefundMoney() * 100));
			
			if(obj.getResult() == true){

				currentOrder.setState(5);
				this.mysqlMgr.getStudentCourseOrderDao().update(currentOrder);

				this.apiResultObject.setSuccess(currentOrder);
				return SUCCESS;
			}
			else{
				this.apiResultObject.setFailure(ApiResultObject.Error, obj.getError());
				return SUCCESS;
			}
			
		}
		else{
			this.apiResultObject.setFailure(ApiResultObject.NotExist, "订单不是该学生的");
			return SUCCESS;
		}
		
	}
	
}
