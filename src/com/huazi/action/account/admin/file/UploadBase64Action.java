package com.huazi.action.account.admin.file;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import s3part.service.AccountService;
import sshbase.struts2.action.FileDataObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;
import sshbase.util.BASE64DecodedMultipartFileUtils;
import sshbase.util.CommonUtil;

import com.huazi.action.account.admin.AdminBaseAction;

public class UploadBase64Action extends AdminBaseAction<FileDataObject> {

	private static final long serialVersionUID = 1L;

	private final static Logger log = LoggerFactory.getLogger(UploadBase64Action.class);

	public UploadBase64Action(){
		log.debug("");
	}

	private String base64File;
	public String getBase64File() {
		return base64File;
	}
	public void setBase64File(String base64File) {
		this.base64File = base64File;
	}
	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug("");
		/** 允许的文件后缀名 */
		//List<String> allowedExtensions = null;
		
		MultipartFile file = BASE64DecodedMultipartFileUtils.base64ToMultipart(base64File);//转文件
		//获取上传的文件的名称(包含后缀)
		String aaaaafilename = file.getOriginalFilename();
		//获取名称(不含后缀)
		String fileName = aaaaafilename.substring(0,aaaaafilename.lastIndexOf("."));
		//获取后缀(扩展名)
		String contentType = aaaaafilename.substring(aaaaafilename.lastIndexOf(".")+1);

		//获取文件后缀(包含".")
		String prefix = aaaaafilename.substring(aaaaafilename.lastIndexOf("."));
		//用uuid作为文件名，防止生成的临时文件重复
		File excelFile = null;
		try {
			excelFile = File.createTempFile(CommonUtil.GetNowTimestamp().getTime()+"", prefix);
			file.transferTo(excelFile);
		} catch (IOException e) {
			excelFile = null;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.getModel().setUpload(excelFile);;
		this.getModel().setUploadContentType(contentType);
		this.getModel().setUploadFileName(aaaaafilename);
		
		
		if(this.getModel().getUploadContentType()== null || this.getModel().getUploadContentType().isEmpty()){
			this.getModel().setUploadContentType("jpg");
		}


		//String extension = FilenameUtils.getExtension(filename);

		this.apiResultObject.setFailure(ApiResultObject.UploadError,"文件不存在,请检查文件参数");
		if(excelFile != null)
		{
			this.apiResultObject.set(AccountService.uploadFile(this.getLoginToken(),"/editor/file", this.getModel().getUpload(), this.getModel().getUploadFileName(), this.getModel().getUploadContentType()));
			if(apiResultObject.getResult() && apiResultObject.getData() != null){
			}
		}
		
		
		return SUCCESS;
	}

}
