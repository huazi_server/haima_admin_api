package com.huazi.action.account.admin.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.huazi.action.account.admin.AdminBaseAction;

public class FileBaseAction<T> extends AdminBaseAction<T> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FileBaseAction.class);

	public FileBaseAction() {
		log.debug("");
	}

	@Override
	public String execute() {
		log.debug("");
		return SUCCESS;
	}
	

	private Long fileId;

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

}
