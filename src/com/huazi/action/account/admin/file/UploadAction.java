package com.huazi.action.account.admin.file;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import s3part.service.AccountService;
import sshbase.struts2.action.FileDataObject;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;
import sshbase.struts2.result.ApiResultObject;

import com.huazi.action.account.admin.AdminBaseAction;

public class UploadAction extends AdminBaseAction<FileDataObject> {

	private static final long serialVersionUID = 1L;

	private final static Logger log = LoggerFactory.getLogger(UploadAction.class);

	public UploadAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug("");
		/** 允许的文件后缀名 */
		//List<String> allowedExtensions = null;

		File file = (File)this.getModel().getUpload();
		//String extension = FilenameUtils.getExtension(filename);

		this.apiResultObject.setFailure(ApiResultObject.UploadError,"文件不存在,请检查文件参数");
		if(file != null)
		{
			this.apiResultObject.set(AccountService.uploadFile(this.getLoginToken(),"/admin/file", this.getModel().getUpload(), this.getModel().getUploadFileName(), this.getModel().getUploadContentType()));
			if(apiResultObject.getResult() && apiResultObject.getData() != null){
			}
		}
		
		
		return SUCCESS;
	}


}
