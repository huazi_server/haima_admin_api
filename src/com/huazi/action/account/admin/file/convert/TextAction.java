package com.huazi.action.account.admin.file.convert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import s3part.service.AccountService;
import sshbase.struts2.interceptor.valiject.Valiject;
import sshbase.struts2.interceptor.valiject.ValijectList;

import com.huazi.action.account.admin.file.FileBaseAction;
import com.huazi.mysql.admin.model.Admin;

public class TextAction extends FileBaseAction<Admin> {

	private static final long serialVersionUID = 1L;

	private final static Logger log = LoggerFactory.getLogger(TextAction.class);

	public TextAction(){
		log.debug("");
	}

	@Override
	@ValijectList(list = { @Valiject("loginAdmin")})
	public String execute(){
		log.debug("");
		this.apiResultObject.set(AccountService.textFile(this.getLoginToken(),this.getFileId()));
		return SUCCESS;
	}

}
