package com.huazi.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.opensymphony.xwork2.validator.annotations.ConditionalVisitorFieldValidator;
import com.opensymphony.xwork2.validator.annotations.ConversionErrorFieldValidator;
import com.opensymphony.xwork2.validator.annotations.DateRangeFieldValidator;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.ExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.IntRangeFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.UrlValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.VisitorFieldValidator;

public class ApiAction extends ApiBaseAction<Object> {

	private static final long serialVersionUID = 2L;
	private static final Logger log = LoggerFactory.getLogger(ApiAction.class);

	public ApiAction(){
		log.debug("");
	}

	public String execute() {
		log.debug("");
		CommonUtil.GetNowDirectory(); 
		apiResultObject.setFailure(ApiResultObject.NotExist, null);

		apiResultObject.setSuccess(this.mysqlMgr.getAdminDao().getById(20111249L));
		
		
		/* 
		//this.mysqlMgr.getConfigGradeDao().priority( this.mysqlMgr.getConfigGradeDao().getById(20150729L), 1, " where 1 = 1 ");
		String instring = 
				"SELECT COUNT(DISTINCT a.id) FROM TABLE_NAME_REPLACE a LEFT JOIN question_knowledge qk ON qk.questionId=a.id WHERE qk.knowledgeId in (" 
				+ CommonUtil.GetStringList(this.mysqlMgr.getConfigKnowledgeDao().getIdsByWhere(100L, 200L, null, " where stageId = 20150725 "))
				+ ") AND a.fatherQuestionId=0 AND a.state=3";
		log.debug(instring);
		apiResultObject.setSuccess(this.mysqlMgr.getQuestionDao().executeSqlResult(instring));
		*/
		
		return SUCCESS;
	}/*
	public String language(){

		HttpSession session =(HttpSession)ServletActionContext.getRequest().getSession(); 
		//String url = (String)session.getAttribute("preurl");
		//basevo.setBackurl(url);
		Locale local = new Locale("zh","CN");
		switch(LanguageInt){
			case 0:local = new Locale("zh","CN");break;
			case 1:local = new Locale("zh","TW");break;
			case 2:local = new Locale("en","US");break;
			case 3:local = new Locale("zh","CN");break;
		}
		session.setAttribute("WW_TRANS_I18N_LOCALE", local);*、
		return SUCCESS;
	}*/
	

	@Validations(
		requiredFields={
			@RequiredFieldValidator(fieldName="age",message="年龄输入的值转换错误")
		},
		stringLengthFields={
			@StringLengthFieldValidator(fieldName="password",minLength="8",maxLength="20",message="密码的长度必须大于8小于20个字符")
		},
		emails={
			@EmailValidator(fieldName="email",message="邮件字段的格式不对")
		},
		conversionErrorFields={
			@ConversionErrorFieldValidator(fieldName="age",message="年龄输入的值转换错误")
		},
		intRangeFields={
			@IntRangeFieldValidator(fieldName="age",min="0",max="150",message="年龄范围为0到150")
		},
		urls={
			@UrlValidator(fieldName="homeUrl",message="个人主页的格式不对")
		},
		dateRangeFields={
			@DateRangeFieldValidator(fieldName="birthday",min="1900-01-01",message="日期输入不真确")
		},
		visitorFields={
			@VisitorFieldValidator(fieldName="name",context="name",message="姓名错误：",appendPrefix=true)
		},
		fieldExpressions={
			@FieldExpressionValidator(expression="age>10",fieldName="age",message="年龄不大于10岁")
		},
		expressions={
			@ExpressionValidator(expression="age<10",message="年龄大于10岁")
		},
		conditionalVisitorFields={
			@ConditionalVisitorFieldValidator(expression="age>10",context="name",fieldName="name",appendPrefix=true,message="ConditionVistor:")
		},
		regexFields={
			@RegexFieldValidator(fieldName="name",regex="([\u4e00-\u9fa5][\u4e00-\u9fa5a-zA-Z0-9]{3,20})|([a-zA-Z][a-zA-Z0-9]{5,20})",key="validation.regexFields")
		}
    )
	public String search(){
		log.debug("");
		this.apiResultObject.setSuccess(null);
		return SUCCESS;
	}

	public String create(){
		log.debug("");
		this.apiResultObject.setSuccess(null);
		return SUCCESS;
	}
	
	public String detail(){
		log.debug("");
		this.apiResultObject.setSuccess(null);
		return SUCCESS;
	}
	
	public String update(){
		log.debug("");
		this.apiResultObject.setSuccess(null);
		return SUCCESS;
	}
	
	public String delete(){
		log.debug("");
		this.apiResultObject.setSuccess(null);
		return SUCCESS;
	}

}