package s3part.service;

import java.io.File;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import sshbase.struts2.result.ApiResultObject;
import sshbase.util.CommonUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class AccountService {
	
	private final static Logger log = Logger.getLogger(AccountService.class);

	private static String apiBaseUrl=null;
	private static String productId="20111107";
	private static String productKey="4c2d32ab731a584eb3848cf1sdf6ee65";

	public static void loadConfig(){
		if(apiBaseUrl != null)return;
		apiBaseUrl = CommonUtil.getConfig("serviceBaseUrl");
	}

	public static ApiResultObject detailOrder(String loginToken,String tradeNo) {
		loadConfig();
		String apiurl = "account/privacy/trade/detail";
		ApiResultObject apiResultObject = new ApiResultObject();
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("tradeNo",tradeNo);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}
	public static ApiResultObject refundOrder(String loginToken,String tradeNo,Integer refundAmount) {
		loadConfig();
		String apiurl = "account/privacy/trade/refund";
		ApiResultObject apiResultObject = new ApiResultObject();
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("tradeNo",tradeNo);
			b.setParameter("refundAmount", refundAmount.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}
	
	public static ApiResultObject resetAccount(String loginToken,Long accountId,String pwd,String type) {
		loadConfig();
		String apiurl = "account/privacy/account/password/reset";
		ApiResultObject apiResultObject = new ApiResultObject();
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("accountId", accountId.toString());
			b.setParameter("accountType", type);
			b.setParameter("password", pwd);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}
	public static ApiResultObject createAccount(String loginToken,String phone,String pwd,String type) {
		loadConfig();
		String apiurl = "account/privacy/account/create";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("phone", phone);
			b.setParameter("password", pwd);
			b.setParameter("accountType", type);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}

	public static ApiResultObject loginAccount(String phone,String pwd,String type) {
		loadConfig();
		String apiurl = "account/common/login";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("phone", phone);
			b.setParameter("password", pwd);
			b.setParameter("accountType", type);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}
	public static ApiResultObject textFile(String loginToken,Long fileId){
		loadConfig();
		String apiurl = "account/privacy/folder/file/text";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("fileId",fileId.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}

	public static ApiResultObject getLogin(Long loginId,String loginToken) {
		loadConfig();
		String apiurl = "account/common/check";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl );
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Account Service down 2");
		}
		return apiResultObject;
	}
	public static ApiResultObject requestVerify(String loginToken,String type){
		loadConfig();
		String apiurl = "account/common/verify/request";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("verifyType",type.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}
	public static ApiResultObject verifyCode(String loginToken,String type,String code){
		loadConfig();
		String apiurl = "account/common/verify/confirm";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("verifyType",type.toString());
			b.setParameter("verifyCode",code.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}
	public static ApiResultObject checkVerify(String loginToken,String type){
		loadConfig();
		String apiurl = "account/common/verify/check";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("verifyType",type.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}

	public static ApiResultObject getSso(String loginToken,Long ssoId){
		loadConfig();
		String apiurl = "account/common/sso/info";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("ssoId",ssoId.toString());
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}
	
	
	
	public static ApiResultObject getApi(Long loginId,String loginToken,String apiurl) {
		HttpServletRequest httprequest = ServletActionContext.getRequest();
		ApiResultObject apiResultObject = new ApiResultObject();
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl + "  Attribute = "+ JSON.toJSON(httprequest.getParameterMap()));
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			for(Entry<String, String[]> names : httprequest.getParameterMap().entrySet()){
				for(String value : names.getValue()){
					b.addParameter(names.getKey() , value);
				}
			}
			b.setParameter("loginToken", loginToken);
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}


	public static ApiResultObject transferApi(String apiurl) {
		HttpServletRequest httprequest = ServletActionContext.getRequest();
		ApiResultObject apiResultObject = new ApiResultObject();
		log.debug("ApiService getApi! apiurl = " + apiurl + "  Attribute = "+ JSON.toJSON(httprequest.getParameterMap()));
		try {
			URIBuilder b = new URIBuilder(apiurl);
			for(Entry<String, String[]> names : httprequest.getParameterMap().entrySet()){
				for(String value : names.getValue()){
					b.addParameter(names.getKey() , value);
				}
			}
			postApi(b,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}

	public static void postApi(URIBuilder b,ApiResultObject apiResultObject) {

		try {
			b.setParameter("loginProductId", productId);
			b.setParameter("loginProductKey",productKey);
			log.debug("ApiService getApi! parameter = " + JSON.toJSON(b.getQueryParams()));
			HttpPost request = new HttpPost(b.build()); 
			getApiPost(request,apiResultObject);
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 1111");
		}
	}
	public static ApiResultObject uploadFile(String loginToken,String folderPath,File file,String filename,String contentType){
		loadConfig();

		String apiurl = "account/privacy/folder/file/upload";
		log.debug("ApiService getApi! apiurl = " + apiBaseUrl + "/" + apiurl);
		ApiResultObject apiResultObject = new ApiResultObject();
		try {
			URIBuilder b = new URIBuilder(apiBaseUrl + "/"+apiurl);
			b.setParameter("loginToken", loginToken);
			b.setParameter("folderPath",folderPath);
			b.setParameter("fileName",filename);
			b.setParameter("contentType",contentType);

			b.setParameter("loginProductId", productId);
			b.setParameter("loginProductKey",productKey);
			
			log.debug("ApiService getApi! parameter = " + JSON.toJSON(b.getQueryParams()));
			HttpPost request = new HttpPost(b.build());
			MultipartEntity reqEntity = new MultipartEntity();
			FileBody bin = new FileBody(file); 
			reqEntity.addPart("upload", bin);

			request.setEntity(reqEntity);
			getApiPost(request,apiResultObject);
            
		} catch (Exception e) {
			apiResultObject.setFailure(apiResultObject.ServerDown,"Server down 2222");
		}
		return apiResultObject;
	}
	private static void getApiPost(HttpPost request,ApiResultObject apiResultObject) {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		try {
			CloseableHttpResponse response = httpClient.execute(request);  
			response.getStatusLine().getStatusCode();
	        HttpEntity entity = response.getEntity();  
	        System.out.println("----------------------------------------");  
	        System.out.println(response.getStatusLine());  
	        System.out.println(entity);  
	        JSONObject data;
			data = (JSONObject) JSON.parse(EntityUtils.toByteArray(entity));
	        System.out.println(data);
			apiResultObject.setType(data.getString("type"));
			apiResultObject.setResult(data.getBoolean("result"));
			apiResultObject.setCode(data.getInteger("code"));
			apiResultObject.setMessage(data.getString("message"));
			apiResultObject.setData(data.get("data"));
			apiResultObject.setError(data.getString("error"));
	        response.close();  
		} catch (Exception e) {
			apiResultObject.setFailure(ApiResultObject.ServerDown,"Server down 1111");
		}
            
	}

	

}
