package s3part.baidu;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.json.JSONObject;

import com.baidu.aip.ocr.AipOcr;


public class BaiduImageUtil {
	 //设置APPID/AK/SK
    public static final String APP_ID = "11172164";
    public static final String API_KEY = "2GpI2dCtL7pFSqGPtIWxmkOn";
    public static final String SECRET_KEY = "oE0WPlhdDkqTnFRv9wedimAVbs47dslp";

    public static JSONObject getWenziUrl(String url) {
        // 初始化一个AipOcr
    	AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

    	  // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("recognize_granularity", "big");
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("vertexes_location", "false");
        options.put("probability", "true");


        // 参数为本地图片二进制数组
        JSONObject res = client.generalUrl(url, options);
        System.out.println(res.toString(2));

        return res;

    }
    
    public static JSONObject getWenzi(File image) {
        // 初始化一个AipOcr
    	AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

    	  // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("recognize_granularity", "big");
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("vertexes_location", "false");
        options.put("probability", "true");


        // 参数为本地图片二进制数组
        byte[] file = changeFileToByte(image);
        JSONObject res = client.general(file, options);
        System.out.println(res.toString(2));

        return res;

    }
    
    private static byte[] changeFileToByte(File fileZip)  
    {  
        byte[] buffer = null;  
        try  
        {   
            if (!fileZip.exists())  
            {  
                return null;  
            }  
            FileInputStream fis = new FileInputStream(fileZip);  
              
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);  
              
            byte[] b = new byte[1000];  
            int n;  
            //每次从fis读1000个长度到b中，fis中读完就会返回-1  
            while ((n = fis.read(b)) != -1)  
            {  
                bos.write(b, 0, n);  
            }  
            fis.close();  
            bos.close();  
            buffer = bos.toByteArray();  
        }  
        catch (FileNotFoundException e)  
        {  
            e.printStackTrace();  
        }  
        catch (IOException e)  
        {  
            e.printStackTrace();  
        }  
        return buffer;  
    }  
    

	public static File erzhi(File file) throws IOException{
		BufferedImage image = ImageIO.read(file);  
        int w = image.getWidth();  
        int h = image.getHeight();  
        float[] rgb = new float[3];  
        double[][] zuobiao = new double[w][h];  
        int R = 0;  
        float red = 0;  
        float green = 0;  
        float blue = 0;  
        BufferedImage bi= new BufferedImage(w, h,  
                BufferedImage.TYPE_BYTE_BINARY);;  
        for (int x = 0; x < w; x++) {  
            for (int y = 0; y < h; y++) {  
                int pixel = image.getRGB(x, y);   
                rgb[0] = (pixel & 0xff0000) >> 16;  
                rgb[1] = (pixel & 0xff00) >> 8;  
                rgb[2] = (pixel & 0xff);  
                red += rgb[0];  
                green += rgb[1];  
                blue += rgb[2];  
                R = (x+1) *(y+1);  
                float avg = (rgb[0]+rgb[1]+rgb[2])/3;  
                zuobiao[x][y] = avg;      
                  
            }  
        }  
        double SW = 170;  
        for (int x = 0; x < w; x++) {  
            for (int y = 0; y < h; y++) {  
                if (zuobiao[x][y] <= SW) {  
                    int max = new Color(0, 0, 0).getRGB();  
                    bi.setRGB(x, y, max);  
                }else{  
                    int min = new Color(255, 255, 255).getRGB();  
                    bi.setRGB(x, y, min);  
                }  
            }             
        }  
          
        ImageIO.write(bi, "jpg", file);  
        return file;
	}


}
